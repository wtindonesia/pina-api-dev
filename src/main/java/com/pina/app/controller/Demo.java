package com.pina.app.controller;

import java.net.URI;
import java.util.List;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.pina.app.exception.ResourceNotFoundException;
import com.pina.app.model.DemoProfile;
import com.pina.app.payload.request.DemoRequest;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.repository.UserRep;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import com.pina.app.service.DemoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;



        
@RestController
@RequestMapping("/api/demo")
public class Demo{

    private final Logger logger = LoggerFactory.getLogger(Demo.class);

    @Autowired
    private DemoService demoService;

    @Autowired
    private UserRep userRepository;


    @GetMapping("/id")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getDemoById(@CurrentUser UserPrincipal currentUser) {
        
        return demoService.getDemoById(currentUser);
    }
    
    
    @PostMapping
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> postDemo(@Valid @RequestBody DemoRequest body, @CurrentUser UserPrincipal currentUser) {

         return demoService.postDemo(body, currentUser);
    }

    @GetMapping
    public ResponseEntity<?> getAllByKey(@RequestParam(value = "key", defaultValue = "") String key) {
        return demoService.getAllByKey(key);
    }
}