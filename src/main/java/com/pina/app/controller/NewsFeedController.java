/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.payload.request.CommentCreateRequest;
import com.pina.app.payload.request.NewsFeedCreateRequest;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import com.pina.app.service.FileService;
import com.pina.app.service.NewsFeedService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Hp
 */
@RestController
@RequestMapping("/api/newsfeed")
public class NewsFeedController {

    @Autowired
    private NewsFeedService newsFeedService;

    @Autowired
    private FileService fileService;

    @PostMapping
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createNewsFeed(@Valid @RequestBody NewsFeedCreateRequest body, @CurrentUser UserPrincipal currentUser) {
        return newsFeedService.createNewsFeed(body, currentUser);
    }

    @PostMapping("/comment")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> commentNewsFeed(@Valid @RequestBody CommentCreateRequest body, @CurrentUser UserPrincipal currentUser) {
        return newsFeedService.createComment(body, currentUser);
    }

    @GetMapping("/{newsFeedId}/comment")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getAllCommentNewsFeed(@PathVariable("newsFeedId") long newsFeedId, 
            @RequestParam(value = "lastId", defaultValue = "0") long lastId,
            @CurrentUser UserPrincipal currentUser
    ) {
        return newsFeedService.getAllCommentNewsFeed(newsFeedId, lastId);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> deteleStock(@PathVariable("id") long id) {
        return newsFeedService.deleteNewsFeed(id);
    }

    @GetMapping()
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> findAll(@RequestParam(value = "lastId", defaultValue = "0") long lastId,
            @CurrentUser UserPrincipal currentUser
    ) {
        return newsFeedService.getAllNewsFeed(currentUser.getId(), lastId);
    }

    @GetMapping("/reload")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> reload(@CurrentUser UserPrincipal currentUser
    ) {
        return newsFeedService.reload(currentUser.getId());
    }

    @PostMapping("/image")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> uploadImage(@RequestParam("file") MultipartFile file, @CurrentUser UserPrincipal currentUser) {
        return fileService.upload(file, "images/nf", currentUser);
    }

    @PostMapping("/file")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file, @CurrentUser UserPrincipal currentUser) {
        return fileService.upload(file, "files/nf", currentUser);
    }
    
    //api get image ada di FileController.java
}
