package com.pina.app.controller;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.model.Preference;
import com.pina.app.model.User;
import com.pina.app.payload.UserIdentityAvailability;
import com.pina.app.payload.UserSummary;
import com.pina.app.payload.request.PreferenceRequest;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.UserProfileResponse;
import com.pina.app.security.UserPrincipal;
import com.pina.app.service.PollService;
import com.pina.app.security.CurrentUser;
import com.pina.app.service.UserService;
import com.pina.app.util.DateFormatUtil;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.pina.app.repository.PollRep;
import com.pina.app.repository.UserRep;
import com.pina.app.repository.VoteRep;
import com.pina.app.util.ModelMapper;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserRep userRepository;

    @Autowired
    private PollRep pollRepository;

    @Autowired
    private VoteRep voteRepository;

    @Autowired
    private PollService pollService;

    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/me")
    @PreAuthorize("hasRole('USER')")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        User user = userOpt.get();
        UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getName(), user.getUserProfile().getFillState(), user.getUserProfile().getStarterState());
        return userSummary;
    }

    @GetMapping("/checkUsernameAvailability")
    public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
        Boolean isAvailable = !userRepository.existsByUserName(username);
        return new UserIdentityAvailability(isAvailable);
    }

    @GetMapping("/checkEmailAvailability")
    public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
        Boolean isAvailable = !userRepository.existsByEmail(email);
        return new UserIdentityAvailability(isAvailable);
    }

    ///=========================================
    @GetMapping("/profile")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getUserProfile(@CurrentUser UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }

        User user = userOpt.get();
        UserProfileResponse userProfile = new UserProfileResponse();
        userProfile.setUserId(user.getId());
        userProfile.setUserName(user.getUserName());
        userProfile.setEmail(user.getEmail());
        userProfile.setRealAccount(user.getRealAccount());
        userProfile.setRealAccountActive(user.getRealAccountActive());
        userProfile.setPinCreated(user.getPinCreated());
        userProfile.setInfluencer(user.getInfluencer());

        if (user.getUserProfile() != null) {
            userProfile.setFillState(user.getUserProfile().getFillState());
            userProfile.setFullName(user.getUserProfile().getFullName());
            userProfile.setImgAvatarUrl(user.getUserProfile().getImgAvatarUrl());
        }

        userProfile.setFillState(0);
        userProfile.setLastLogin(DateFormatUtil.format(user.getLastLogin()));
        return ResponseEntity.ok(new ApiResponse(true, null, "", userProfile));
    }

    @GetMapping("/agenda")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getUserAgenda(@CurrentUser UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }
        User user = userOpt.get();
        return ResponseEntity.ok(new ApiResponse(true, null, "", ModelMapper.mapAgendaResponse(user.getAgendas())));
    }

    @PostMapping("/applyinfluencer")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> applyAsInfluencer(@CurrentUser UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }

        User user = userOpt.get();
        user.setApplyAsInfluencer(true);
        userRepository.save(user);
        return ResponseEntity.ok(new ApiResponse(true, null, "Send apply influencer is success", null));
    }

    @GetMapping("/preference")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getUserPreferece(@CurrentUser UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }

        User user = userOpt.get();
        if (user.getPreference() == null) {
            user.setPreference(new Preference());
        }

        return ResponseEntity.ok(new ApiResponse(true, null, "", user.getPreference()));
    }

    @PutMapping("/preference")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> updateUserPreference(@Valid @RequestBody PreferenceRequest body,
            @CurrentUser UserPrincipal currentUser) {

        Optional<User> userOpt = userRepository.findById(currentUser.getId());

        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }
        User user = userOpt.get();
        Preference p = new Preference();
        if (user.getPreference() != null) {
            p = user.getPreference();

        }

        p.setSettings(body.getSettings());
        user.setPreference(p);
        userRepository.save(user);

        return ResponseEntity.ok(new ApiResponse(true, null, "update preference success", null));
    }

    @PostMapping("/starter")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> updateStarterState(@CurrentUser UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());

        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }

        User user = userOpt.get();
        user.getUserProfile().setStarterState(1);
        userRepository.save(user);
        logger.info("USER: " + user.getId());

        return ResponseEntity.ok(new ApiResponse(true, null, "update starter success", null));
    }

}
