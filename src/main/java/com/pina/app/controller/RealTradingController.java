/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.payload.request.LoginPinRequest;
import com.pina.app.payload.request.PinAddRequest;
import com.pina.app.payload.request.WithdrawRequest;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import com.pina.app.service.RealTradingService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Hp
 */
@RestController
@RequestMapping("/api/trading")
public class RealTradingController {

    @Autowired
    RealTradingService realTradingService;

    @PostMapping("/login")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> loginPin(@Valid @RequestBody LoginPinRequest body, @CurrentUser UserPrincipal currentUser) {
        return realTradingService.loginPin(body, currentUser.getId());
    }

    @PostMapping("/logout")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> logoutPin(@CurrentUser UserPrincipal currentUser) {
        return realTradingService.logoutPin(currentUser.getId());
    }

    @PostMapping("/pin")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createPin(@RequestBody PinAddRequest body, @CurrentUser UserPrincipal currentUser) {
        return realTradingService.createPin(body, currentUser.getId());
    }

    @GetMapping("/portfolio")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getPortfolio(@CurrentUser UserPrincipal currentUser) {
        return realTradingService.getPortfolio(currentUser.getId());
    }

    @GetMapping("/deposit")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getDeposit(@CurrentUser UserPrincipal currentUser) {
        return realTradingService.getDeposit(currentUser.getId());
    }

    @GetMapping("/withdraw")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getWithdraw(@CurrentUser UserPrincipal currentUser) {
        return realTradingService.getWithdraw(currentUser.getId());
    }

    @PostMapping("/withdraw")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> reqWithdraw(@RequestBody WithdrawRequest body, @CurrentUser UserPrincipal currentUser) {
        return realTradingService.reqWithdraw(body, currentUser.getId());
    }
}
