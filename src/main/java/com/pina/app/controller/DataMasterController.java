/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.exception.BadRequestException;
import com.pina.app.model.Bank;
import com.pina.app.model.City;
import com.pina.app.model.Kecamatan;
import com.pina.app.model.Kelurahan;
import com.pina.app.model.Province;
import com.pina.app.payload.request.master.CityRequest;
import com.pina.app.payload.request.master.CityUpdateRequest;
import com.pina.app.payload.request.master.KecamatanRequest;
import com.pina.app.payload.request.master.KecamatanUpdateRequest;
import com.pina.app.payload.request.master.KelurahanRequest;
import com.pina.app.payload.request.master.KelurahanUpdateRequest;
import com.pina.app.payload.request.master.ProvinceRequest;
import com.pina.app.payload.response.*;
import com.pina.app.repository.BankRep;
import com.pina.app.repository.CityRep;
import com.pina.app.repository.KecamatanRep;
import com.pina.app.repository.KelurahanRep;
import com.pina.app.repository.ProvinceRep;
import com.pina.app.util.ModelMapper;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Hp
 */
@RestController
@RequestMapping("/api")
public class DataMasterController {

    @Autowired
    private ProvinceRep provinceRep;

    @Autowired
    private CityRep cityRep;

    @Autowired
    private KecamatanRep kecRep;

    @Autowired
    private KelurahanRep kelRep;

    @Autowired
    private BankRep bankRep;

    //==========================================================================
    //             PROVINCE
    //==========================================================================
    @GetMapping("/province/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getProvince(@PathVariable("id") int id) {
        Optional<Province> provOpt = provinceRep.findById(id);
        if (!provOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Province not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(new ApiResponse(true, null, "", provOpt.get()));
    }

    @GetMapping("/province")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getAllProvince(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @RequestParam(value = "sort", defaultValue = "asc") String sort,
            @RequestParam(value = "sort_by", defaultValue = "name") String sortby,
            @RequestParam(value = "by", defaultValue = "", required = true) String by) {
        Sort.Direction sortDirection = Sort.Direction.ASC;
        if (sort.toLowerCase().equals("asc")) {
            sortDirection = Sort.Direction.ASC;
        } else {
            sortDirection = Sort.Direction.DESC;
        }
        Pageable pageable = PageRequest.of(page, size, sortDirection, sortby);
        Page<Province> list = provinceRep.findAll(pageable);

        List<LocationResponse> resList = list.map(province -> {
            LocationResponse p = new LocationResponse();
            p.setId(province.getId());
            p.setName(province.getName());
            return p;
        }).getContent();

        PagedResponse<LocationResponse> r = new PagedResponse<>(resList, list.getNumber(),
                list.getSize(), list.getTotalElements(), list.getTotalPages(), list.isLast());

        return ResponseEntity.ok(new ApiResponse(true, null, "", r));

    }

    @PostMapping("/province")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createProvince(@Valid @RequestBody ProvinceRequest body) {
        Province prov = null;
        try {
            prov = provinceRep.save(new Province(body.getName()));
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("Duplicate Data");
        }

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(prov.getId()).toUri();
        return ResponseEntity.created(location).body(new ApiResponse(true, null, "Province Created", null));
    }

    @PutMapping("/province/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> updateProvince(@PathVariable("id") int id, @Valid @RequestBody ProvinceRequest body) {
        Optional<Province> provOpt = provinceRep.findById(id);
        if (!provOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Province not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        Province prov = provOpt.get();
        prov.setName(body.getName());
        provinceRep.save(prov);
        return ResponseEntity.ok(new ApiResponse(true, null, "update success", null));
    }

    @DeleteMapping("/province/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> deleteProvince(@PathVariable("id") int id) {
        Optional<Province> provOpt = provinceRep.findById(id);
        if (!provOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Province not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        Province prov = provOpt.get();
        provinceRep.delete(prov);

        return ResponseEntity.ok(new ApiResponse(true, null, "delete success", null));
    }

    //==========================================================================
    //             CITY
    //==========================================================================
    @GetMapping("/province/{id}/cities")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getCityByProvince(@PathVariable("id") int id) {
        List<City> cities = cityRep.findAllByProvinceId(id);

        List<LocationResponse> resList = cities.stream().map(city -> {
            LocationResponse locRes = new LocationResponse();
            locRes.setId(city.getId());
            locRes.setName(city.getName());
            return locRes;
        }).collect(Collectors.toList());

        return ResponseEntity.ok(new ApiResponse(true, null, "", resList));
    }

    @GetMapping("/city/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getCity(@PathVariable("id") int id) {
        Optional<City> cityOpt = cityRep.findById(id);
        if (!cityOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "City not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        CityResponse res = ModelMapper.mapCityResponse(cityOpt.get());
        return ResponseEntity.ok(new ApiResponse(true, null, "", res));

    }

    @GetMapping("/city")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getAllCity(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @RequestParam(value = "sort", defaultValue = "asc") String sort,
            @RequestParam(value = "sort_by", defaultValue = "name") String sortby,
            @RequestParam(value = "by", defaultValue = "", required = true) String by) {
        Sort.Direction sortDirection = Sort.Direction.ASC;
        if (sort.toLowerCase().equals("asc")) {
            sortDirection = Sort.Direction.ASC;
        } else {
            sortDirection = Sort.Direction.DESC;
        }
        Pageable pageable = PageRequest.of(page, size, sortDirection, sortby);
        Page<City> list = cityRep.findAll(pageable);

        List<LocationResponse> resList = list.map(city -> {
            LocationResponse p = new LocationResponse();
            p.setId(city.getId());
            p.setName(city.getName());
            return p;
        }).getContent();

        PagedResponse<LocationResponse> r = new PagedResponse<>(resList, list.getNumber(),
                list.getSize(), list.getTotalElements(), list.getTotalPages(), list.isLast());

        return ResponseEntity.ok(new ApiResponse(true, null, "", r));

    }

    @PostMapping("/city")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createCity(@Valid @RequestBody CityRequest body) {

        Optional<Province> provOpt = provinceRep.findById(body.getProviceId());
        if (!provOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Province not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        City city = null;
        try {
            city = cityRep.save(new City(provOpt.get(), body.getName()));
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("Duplicate City name");
        }

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(city.getId()).toUri();
        return ResponseEntity.created(location).body(new ApiResponse(true, null, "City Created", null));
    }

    @PutMapping("/city/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> updateCity(@PathVariable("id") int id, @Valid @RequestBody CityUpdateRequest body) {

        Optional<City> cityOpt = cityRep.findById(id);
        if (!cityOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "City not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        City city = cityOpt.get();
        city.setName(body.getName());

        if (body.getProviceId() != null) {
            Optional<Province> provOpt = provinceRep.findById(body.getProviceId());
            if (!provOpt.isPresent()) {
                return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Province not found!", null),
                        HttpStatus.NOT_FOUND);
            }
            city.setProvince(provOpt.get());
        }

        try {
            cityRep.save(city);
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("Duplicate Data");
        }

        return ResponseEntity.ok(new ApiResponse(true, null, "update success", null));
    }

    @DeleteMapping("/city/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> deleteCity(@PathVariable("id") int id) {
        Optional<City> cityOpt = cityRep.findById(id);
        if (!cityOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "City not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        City prov = cityOpt.get();
        cityRep.delete(prov);

        return ResponseEntity.ok(new ApiResponse(true, null, "delete success", null));
    }

    //==========================================================================
    //             KECAMATAN
    //==========================================================================
    @GetMapping("/city/{id}/kecamatan")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getKecamatanByCity(@PathVariable("id") int id) {
        List<Kecamatan> kecamatans = kecRep.findKecamatanByCityId(id);

        List<LocationResponse> resList = kecamatans.stream().map(kecamatan -> {
            LocationResponse locRes = new LocationResponse();
            locRes.setId(kecamatan.getId());
            locRes.setName(kecamatan.getName());
            return locRes;
        }).collect(Collectors.toList());

        return ResponseEntity.ok(new ApiResponse(true, null, "", resList));
    }

    @GetMapping("/kecamatan/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getKecamatan(@PathVariable("id") int id) {
        Optional<Kecamatan> kecOpt = kecRep.findById(id);
        if (!kecOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Kecamatan not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        KecamatanResponse res = ModelMapper.mapKecamatanResponse(kecOpt.get());
        return ResponseEntity.ok(new ApiResponse(true, null, "", res));

    }

    @GetMapping("/kecamatan")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getAllKecamatan(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @RequestParam(value = "sort", defaultValue = "asc") String sort,
            @RequestParam(value = "sort_by", defaultValue = "name") String sortby,
            @RequestParam(value = "by", defaultValue = "", required = true) String by) {
        Sort.Direction sortDirection = Sort.Direction.ASC;
        if (sort.toLowerCase().equals("asc")) {
            sortDirection = Sort.Direction.ASC;
        } else {
            sortDirection = Sort.Direction.DESC;
        }
        Pageable pageable = PageRequest.of(page, size, sortDirection, sortby);
        Page<Kecamatan> list = kecRep.findAll(pageable);

        List<LocationResponse> resList = list.map(city -> {
            LocationResponse p = new LocationResponse();
            p.setId(city.getId());
            p.setName(city.getName());
            return p;
        }).getContent();

        PagedResponse<LocationResponse> r = new PagedResponse<>(resList, list.getNumber(),
                list.getSize(), list.getTotalElements(), list.getTotalPages(), list.isLast());

        return ResponseEntity.ok(new ApiResponse(true, null, "", r));

    }

    @PostMapping("/kecamatan")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createKecamatan(@Valid @RequestBody KecamatanRequest body) {

        Optional<City> cityOpt = cityRep.findById(body.getCityId());
        if (!cityOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Kecamatan not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        Kecamatan kec = null;
        try {
            kec = kecRep.save(new Kecamatan(cityOpt.get(), body.getName()));
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("Duplicate City name");
        }

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(kec.getId()).toUri();
        return ResponseEntity.created(location).body(new ApiResponse(true, null, "Kecamatan Created", null));
    }

    @PutMapping("/kecamatan/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> updateKecamatan(@PathVariable("id") int id, @Valid @RequestBody KecamatanUpdateRequest body) {

        Optional<Kecamatan> kecOpt = kecRep.findById(id);
        if (!kecOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Kecamatan not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        Kecamatan kec = kecOpt.get();
        kec.setName(body.getName());

        if (body.getCityId() != null) {
            Optional<City> cityOpt = cityRep.findById(body.getCityId());
            if (!cityOpt.isPresent()) {
                return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "City not found!", null),
                        HttpStatus.NOT_FOUND);
            }
            kec.setCity(cityOpt.get());
        }

        try {
            kecRep.save(kec);
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("Duplicate Data");
        }

        return ResponseEntity.ok(new ApiResponse(true, null, "update success", null));
    }

    @DeleteMapping("/kecamatan/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> deleteKecamatan(@PathVariable("id") int id) {
        Optional<Kecamatan> kecOpt = kecRep.findById(id);
        if (!kecOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "City not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        Kecamatan kec = kecOpt.get();
        kecRep.delete(kec);

        return ResponseEntity.ok(new ApiResponse(true, null, "delete success", null));
    }

    //==========================================================================
    //             KELURAHAN
    //==========================================================================
    @GetMapping("/kecamatan/{id}/kelurahan")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getKecamatanByKelurahan(@PathVariable("id") int id) {
        List<Kelurahan> kelurahans = kelRep.findAllByKecamatanId(id);

        List<LocationResponse> resList = kelurahans.stream().map(kelurahan -> {
            LocationResponse locRes = new LocationResponse();
            locRes.setId(kelurahan.getId());
            locRes.setName(kelurahan.getName());
            return locRes;
        }).collect(Collectors.toList());

        return ResponseEntity.ok(new ApiResponse(true, null, "", resList));
    }

    @GetMapping("/kelurahan/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getKelurahan(@PathVariable("id") int id) {
        Optional<Kelurahan> kecOpt = kelRep.findById(id);
        if (!kecOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "kelurahan not found!", null),
                    HttpStatus.NOT_FOUND);
        }
        KelurahanResponse res = ModelMapper.mapKelurahanResponse(kecOpt.get());
        return ResponseEntity.ok(new ApiResponse(true, null, "", res));

    }

    @GetMapping("/kelurahan")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getAllKelurahan(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @RequestParam(value = "sort", defaultValue = "asc") String sort,
            @RequestParam(value = "sort_by", defaultValue = "name") String sortby,
            @RequestParam(value = "by", defaultValue = "", required = true) String by) {
        Sort.Direction sortDirection = Sort.Direction.ASC;
        if (sort.toLowerCase().equals("asc")) {
            sortDirection = Sort.Direction.ASC;
        } else {
            sortDirection = Sort.Direction.DESC;
        }
        Pageable pageable = PageRequest.of(page, size, sortDirection, sortby);
        Page<Kelurahan> list = kelRep.findAll(pageable);
        List<LocationResponse> resList = list.map(kel -> {
            LocationResponse p = new LocationResponse();
            p.setId(kel.getId());
            p.setName(kel.getName());
            return p;
        }).getContent();

        PagedResponse<LocationResponse> r = new PagedResponse<>(resList, list.getNumber(),
                list.getSize(), list.getTotalElements(), list.getTotalPages(), list.isLast());

        return ResponseEntity.ok(new ApiResponse(true, null, "", r));

    }

    @PostMapping("/kelurahan")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createKelurahan(@Valid @RequestBody KelurahanRequest body) {

        Optional<Kecamatan> kecOpt = kecRep.findById(body.getKecamatanId());
        if (!kecOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "kelurahan not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        Kelurahan kel = null;
        try {
            kel = kelRep.save(new Kelurahan(kecOpt.get(), body.getName()));
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("Duplicate Data");
        }

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(kel.getId()).toUri();
        return ResponseEntity.created(location).body(new ApiResponse(true, null, "Kelurahan Created", null));
    }

    @PutMapping("/kelurahan/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> updateKelurahan(@PathVariable("id") int id, @Valid @RequestBody KelurahanUpdateRequest body) {

        Optional<Kelurahan> kelOpt = kelRep.findById(id);
        if (!kelOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Kelurahan not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        Kelurahan kel = kelOpt.get();
        kel.setName(body.getName());

        if (body.getKecamatanId() != null) {
            Optional<Kecamatan> kecOpt = kecRep.findById(body.getKecamatanId());
            if (!kecOpt.isPresent()) {
                return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Kecamatan not found!", null),
                        HttpStatus.NOT_FOUND);
            }
            kel.setKecamatan(kecOpt.get());
        }

        try {
            kelRep.save(kel);
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("Duplicate Data");
        }

        return ResponseEntity.ok(new ApiResponse(true, null, "update success", null));
    }

    @DeleteMapping("/kelurahan/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> deleteKelurahan(@PathVariable("id") int id) {
        Optional<Kelurahan> kecOpt = kelRep.findById(id);
        if (!kecOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Kecamatan not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        Kelurahan kel = kecOpt.get();
        kelRep.delete(kel);

        return ResponseEntity.ok(new ApiResponse(true, null, "delete success", null));
    }

    //==========================================================================
    //             Bank
    //==========================================================================
    @GetMapping("/rdnbank")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getAllRDNBank(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @RequestParam(value = "sort", defaultValue = "asc") String sort,
            @RequestParam(value = "sort_by", defaultValue = "name") String sortby,
            @RequestParam(value = "by", defaultValue = "", required = true) String by) {
        Sort.Direction sortDirection = Sort.Direction.ASC;
        if (sort.toLowerCase().equals("asc")) {
            sortDirection = Sort.Direction.ASC;
        } else {
            sortDirection = Sort.Direction.DESC;
        }
        Pageable pageable = PageRequest.of(page, size, sortDirection, sortby);
        Page<Bank> list = bankRep.findAll(pageable);
        List<BankResponse> resList = list.map(bank -> {
            BankResponse p = new BankResponse();
            p.setId(bank.getId());
            p.setName(bank.getName());
            p.setLogoUrl(bank.getLogoUrl());
            return p;
        }).getContent();

        PagedResponse<BankResponse> r = new PagedResponse<>(resList, list.getNumber(),
                list.getSize(), list.getTotalElements(), list.getTotalPages(), list.isLast());

        return ResponseEntity.ok(new ApiResponse(true, null, "", r));

    }
}
