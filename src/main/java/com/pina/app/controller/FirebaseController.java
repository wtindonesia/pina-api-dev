/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.model.User;
import com.pina.app.payload.NotificationMsg;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.repository.UserRep;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import com.pina.app.service.FirebaseService;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Hp
 */
@RestController
@RequestMapping("/api/notification")
public class FirebaseController {

    @Autowired
    private UserRep userRepository;

    @Autowired
    private FirebaseService firebaseService;

    @PostMapping("/users/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> notifyUser(@PathVariable("id") long id, @RequestBody NotificationMsg msg, @CurrentUser UserPrincipal currentUser) {
        firebaseService.sendNotificationByUserId("" + id, msg);
        return ResponseEntity.ok(new ApiResponse(true, null, "send notification success", null));

    }

    @PostMapping("/all")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> notifyAll(@RequestBody NotificationMsg msg, @CurrentUser UserPrincipal currentUser) {
        firebaseService.sendNotificationToAll(msg);
        return ResponseEntity.ok(new ApiResponse(true, null, "send notification to all user success", null));

    }

    @GetMapping("/token")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getToken(@CurrentUser UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }
        User user = userOpt.get();
        String token = "";
        try {
            firebaseService.createUserWithUid(user);
            token = firebaseService.createCustomToken("" + currentUser.getId());
        } catch (InterruptedException | ExecutionException ex) {
            Logger.getLogger(FirebaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Map<String, String> map = new HashMap<>();
        map.put("firebaseToken", token);
        return ResponseEntity.ok(new ApiResponse(true, null, "success", map));
    }
}
