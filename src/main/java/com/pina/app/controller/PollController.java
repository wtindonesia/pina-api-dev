package com.pina.app.controller;

import com.pina.app.model.Poll;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.PagedResponse;
import com.pina.app.payload.request.PollRequest;
import com.pina.app.payload.response.PollResponse;
import com.pina.app.payload.request.VoteRequest;
import com.pina.app.payload.request.VotesRequest;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import com.pina.app.service.PollService;
import com.pina.app.util.AppConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.validation.Valid;
import java.net.URI;

/**
 * Created by hp on 20/11/17.
 */
@RestController
@RequestMapping("/api/polls")
public class PollController {

    private static final Logger logger = LoggerFactory.getLogger(PollController.class);

    @Autowired
    private PollService pollService;

    @GetMapping
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getPolls(@CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        PagedResponse<PollResponse> result = pollService.getAllPolls(currentUser, page, size);
        return ResponseEntity.ok(new ApiResponse(true, null, "", result));
    }

    @PostMapping
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createPoll(@Valid @RequestBody PollRequest pollRequest) {
        Poll poll = pollService.createPoll(pollRequest);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{pollId}")
                .buildAndExpand(poll.getId()).toUri();

        return ResponseEntity.created(location)
                .body(new ApiResponse(true, null, "Poll Created Successfully", null));
    }

    @GetMapping("/{pollId}")
    public PollResponse getPollById(@CurrentUser UserPrincipal currentUser,
            @PathVariable Long pollId) {
        return pollService.getPollById(pollId, currentUser);
    }

    @PostMapping("/{pollId}/votes")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> castVote(@CurrentUser UserPrincipal currentUser,
            @PathVariable Long pollId,
            @Valid @RequestBody VoteRequest voteRequest) {
        PollResponse result = pollService.castVoteAndGetUpdatedPoll(pollId, voteRequest, currentUser);
        return ResponseEntity.ok(new ApiResponse(true, null, "Your answers succes submited", null));
    }

    @PostMapping("/votes")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> castVotes(@CurrentUser UserPrincipal currentUser,
            @Valid @RequestBody VotesRequest votesRequest) {
        pollService.updateVotes(votesRequest, currentUser);
        return ResponseEntity.ok(new ApiResponse(true, null, "Your answer succes submited", null));
    }

}
