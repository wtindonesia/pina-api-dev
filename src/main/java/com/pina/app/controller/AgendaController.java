/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.exception.BadRequestException;
import com.pina.app.model.Agenda;
import com.pina.app.model.AgendaStatus;
import com.pina.app.model.User;
import com.pina.app.payload.request.AgendaAddRequest;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.PagedResponse;
import com.pina.app.repository.AgendaRep;
import com.pina.app.repository.UserRep;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import java.beans.PropertyEditorSupport;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Hp
 */
@RestController
@RequestMapping("/api")
public class AgendaController {

    @Autowired
    private AgendaRep agendaRep;

    @Autowired
    UserRep userRepository;

    @GetMapping("/agenda")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getAllAgenda(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @RequestParam(value = "sort", defaultValue = "asc") String sort,
            @RequestParam(value = "sort_by", defaultValue = "id") String sortby,
            @RequestParam(value = "by", defaultValue = "", required = true) String by) {
        Sort.Direction sortDirection = Sort.Direction.ASC;
        if (sort.toLowerCase().equals("asc")) {
            sortDirection = Sort.Direction.ASC;
        } else {
            sortDirection = Sort.Direction.DESC;
        }
        Pageable pageable = PageRequest.of(page, size, sortDirection, sortby);
        Page<Agenda> list = agendaRep.findAll(pageable);
        List<Agenda> resList = list.map(agenda -> {
            return agenda;
        }).getContent();
        PagedResponse<Agenda> r = new PagedResponse<>(resList, list.getNumber(),
                list.getSize(), list.getTotalElements(), list.getTotalPages(), list.isLast());

        return ResponseEntity.ok(new ApiResponse(true, null, "", r));

    }

    @PostMapping("/agenda")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createAgenda(@Valid @RequestBody AgendaAddRequest body,
            @CurrentUser UserPrincipal currentUser) {

        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }
        User user = userOpt.get();

        Agenda agenda = new Agenda();
        agenda.setTitle(body.getTitle());
        agenda.setMeeetDate(body.getMeeetDate());
        agenda.setLocation(body.getLocation());
        agenda.setAddress1(body.getAddress1());
        agenda.setAddress2(body.getAddress2());
        agenda.setAdditionalInfo(body.getAdditionalInfo());
        agenda.setStatus(AgendaStatus.NEW);
        agenda.setUser(user);

        try {
            agendaRep.save(agenda);
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("Duplicate Data");
        }

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(agenda.getId()).toUri();
        return ResponseEntity.created(location).body(new ApiResponse(true, null, "Agenda Created", null));
    }

    //digunakan untuk convert string to enum, saat request POST 
    @InitBinder
    public void initBinder(final WebDataBinder webdataBinder) {
        webdataBinder.registerCustomEditor(AgendaStatus.class, new AgendaStatusConverter());
    }
}

class AgendaStatusConverter extends PropertyEditorSupport {

    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
        setValue(AgendaStatus.fromValue(text));
    }

}
