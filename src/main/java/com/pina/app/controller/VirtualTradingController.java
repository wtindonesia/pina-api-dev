/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.model.Stock;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.trading.PortfolioPct;
import com.pina.app.payload.response.trading.PortfolioResponse;
import com.pina.app.repository.StockRep;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import com.pina.app.service.StockService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Hp
 */
@RestController
@RequestMapping("/api/trading/virtual")
public class VirtualTradingController {

    @Autowired
    StockService stockService;

    @Autowired
    private StockRep stockRepository;

    @GetMapping("/portfolio")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getPortfolio(@CurrentUser UserPrincipal currentUser) {

        PortfolioPct portfolioPct = new PortfolioPct();
        portfolioPct.setStockPct(70);
        portfolioPct.setReksadanaPct(20);
        portfolioPct.setCopyFundPct(10);

        List<String> stocks = new ArrayList<>();
        stocks.add("EXCL");
        stocks.add("ISAT");
        stocks.add("TLKM");
        List<Stock> stockList = stockRepository.findByCodeIn(stocks);

        PortfolioResponse res = new PortfolioResponse();
        res.setAvailableCash(700000);
        res.setBalance(1000000);
        res.setValue(500000000);
        res.setProfit(1000000);
        res.setProfitPct(10.72);
        res.setPortfolioPct(portfolioPct);
        res.setStockRecommendation(stockList);

        return ResponseEntity.ok(new ApiResponse(true, null, "", res));

    }
}
