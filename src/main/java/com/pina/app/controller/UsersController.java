/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.model.User;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.PagedResponse;
import com.pina.app.payload.response.PeopleResponse;
import com.pina.app.repository.UserRep;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Hp
 */
@RestController
@RequestMapping("/api/users")
public class UsersController {

    @Autowired
    private UserRep userRepository;

    @GetMapping("")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> findInfluencer(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @RequestParam(value = "sort", defaultValue = "asc") String sort,
            @RequestParam(value = "sort_by", defaultValue = "email") String sortby,
            @CurrentUser UserPrincipal currentUser
    ) {
        Page<User> ls = null;

        if(sort.toLowerCase().equals("rand")) {
            ls = userRepository.findAllInfluencerRandom(new PageRequest(page, size));
        } else {
            Sort.Direction sortDirection = Sort.Direction.ASC;
            if (sort.toLowerCase().equals("asc")) {
                sortDirection = Sort.Direction.ASC;
            } else {
                sortDirection = Sort.Direction.DESC;
            }
            Pageable pageable = PageRequest.of(page, size, sortDirection, sortby);
            ls = userRepository.findAllInfluencerNative(pageable, true);
        }

        List<PeopleResponse> resList = ls.map(u -> {
            PeopleResponse people = new PeopleResponse();
            people.setUserId(u.getId());
            people.setName(u.getName());
            people.setInfluencer(u.getInfluencer());
            people.setPercent("20%");
            people.setPhoto("https://pbs.twimg.com/profile_images/491390735749378050/ZK5nwUPR_400x400.jpeg");
            people.setReturns("Return (Last 1294)");
            return people;
        }).getContent();

        PagedResponse<PeopleResponse> result = new PagedResponse<>(resList, ls.getNumber(),
                ls.getSize(), ls.getTotalElements(), ls.getTotalPages(), ls.isLast());

        return ResponseEntity.ok(new ApiResponse(true, null, "", result));
    }
}
