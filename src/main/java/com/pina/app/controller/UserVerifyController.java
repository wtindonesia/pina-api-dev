/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.model.Bank;
import com.pina.app.model.UserSurvey;
import com.pina.app.model.User;
import com.pina.app.model.UserProfile;
import com.pina.app.payload.request.*;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.MediaResponse;
import com.pina.app.repository.BankRep;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import com.pina.app.service.UserProfileService;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.pina.app.repository.UserRep;

/**
 *
 * @author Hp
 */
@RestController
@RequestMapping("/api")
public class UserVerifyController {

    @Autowired
    private UserProfileService userProfileService;

    @Autowired
    UserRep userRepository;
    
    @Autowired
    private BankRep bankRep;

    
    @PostMapping("/verify")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> verify(@Valid @RequestBody VerifyRequest body,
            @CurrentUser UserPrincipal currentUser) {

        Optional<User> userOpt = userRepository.findById(currentUser.getId());

        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }
        User user = userOpt.get();
        UserProfile userProfile = user.getUserProfile();
        if (userProfile == null) {
            userProfile = new UserProfile();
            userProfile.setUser(user);
        }
        user.setRealAccount(true);
        userProfile.setImgFaceUrl(body.getImgFaceUrl());
        userProfile.setImgKtpUrl(body.getImgKtpUrl());
        userProfile.setImgNpwpUrl(body.getImgNpwpUrl());
        userProfile.setImgSignatureUrl(body.getImgSignatureUrl());
        userProfile.setFillState(1);
        userProfile.setUser(user);
        userProfile.setAddress(body.getAddress());
        userProfile.setBirthDate(body.getBirthDate());
        userProfile.setBirthPlace(body.getBirthPlace());
        userProfile.setCityId(body.getCityId());
        userProfile.setFullName(body.getFullName());
        userProfile.setKecId(body.getKecId());
        userProfile.setKelId(body.getKelId());
        userProfile.setKtpNo(body.getKtpNo());
        userProfile.setKtpExpired(body.getKtpExpired());
        userProfile.setKtpLifetime(body.getKtpLifetime());
        userProfile.setNationality(body.getNationality());
        userProfile.setPhoneNo(body.getPhoneNo());
        userProfile.setProvinceId(body.getProvinceId());
        userProfile.setRtrw(body.getRtrw());
        userProfile.setZipCode(body.getZipCode());
        
        UserSurvey s = new UserSurvey();
        s.setQuestions(body.getQuestions());
        userProfile.setSurvey(s);
        userProfile.setRdnbankAccountHolder(body.getRdnbankAccountHolder());
        userProfile.setRdnbankAccountNo(body.getRdnbankAccountNo());
        userProfile.setRdnbankId(body.getRdnbankId());
        
        userProfileService.update(userProfile);

        return ResponseEntity.ok(new ApiResponse(true, null, "success verify step 1", null));
    }


    /**
     * Get User Data Verify Step 1
     * @author      Sandi Andrian<andrian.sandi@gmail.com>
     * @since       Nov 4, 2019
     **/
    @GetMapping("/verify/step1")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getVerifyStep1(@CurrentUser UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());

        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "User not Found", null),
                    HttpStatus.BAD_REQUEST);
        }
        User user = userOpt.get();
        MediaResponse res = new MediaResponse();
        res.setImageKtpUrl(user.getUserProfile().getImgKtpUrl());
        res.setImageKtpFaceUrl(user.getUserProfile().getImgFaceUrl());
        res.setImageNpwpUrl(user.getUserProfile().getImgNpwpUrl());
        res.setImageSignatureUrl(user.getUserProfile().getImgSignatureUrl());
        return ResponseEntity.ok(new ApiResponse(true, null, "verify data fetched", res));
    }
    
    
    //upload foto
    @PostMapping("/verify/step1")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> verifyStep1(@Valid @RequestBody VerifyStep1Request body,
            @CurrentUser UserPrincipal currentUser) {

        Optional<User> userOpt = userRepository.findById(currentUser.getId());

        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }
        User user = userOpt.get();
        UserProfile userProfile = user.getUserProfile();
        if (userProfile == null) {
            userProfile = new UserProfile();
            userProfile.setUser(user);
        }

        userProfile.setImgFaceUrl(body.getImgFaceUrl());
        userProfile.setImgKtpUrl(body.getImgKtpUrl());
        userProfile.setImgNpwpUrl(body.getImgNpwpUrl());
        userProfile.setImgSignatureUrl(body.getImgSignatureUrl());
        userProfile.setFillState(1);
        userProfile.setUser(user);
        
        userProfileService.update(userProfile);

        return ResponseEntity.ok(new ApiResponse(true, null, "success verify step 1", null));
    }

    //data user
    @PostMapping("/verify/step2")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> verifyStep2(@Valid @RequestBody VerifyStep2Request body,
            @CurrentUser UserPrincipal currentUser) {

        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }
        User user = userOpt.get();
        UserProfile userProfile = user.getUserProfile();
        if (userProfile == null) {
            userProfile = new UserProfile();
            userProfile.setUser(user);
        }

        userProfile.setAddress(body.getAddress());
        userProfile.setBirthDate(body.getBirthDate());
        userProfile.setBirthPlace(body.getBirthPlace());
        userProfile.setCityId(body.getCityId());
        userProfile.setFullName(body.getFullName());
        userProfile.setKecId(body.getKecId());
        userProfile.setKelId(body.getKelId());
        userProfile.setKtpNo(body.getKtpNo());
        userProfile.setKtpExpired(body.getKtpExpired());
        userProfile.setKtpLifetime(body.getKtpLifetime());
        userProfile.setNationality(body.getNationality());
        userProfile.setPhoneNo(body.getPhoneNo());
        userProfile.setProvinceId(body.getProvinceId());
        userProfile.setRtrw(body.getRtrw());
        userProfile.setZipCode(body.getZipCode());
        userProfile.setFillState(2);
        userProfileService.update(userProfile);

        return ResponseEntity.ok(new ApiResponse(true, null, "success verify step 2", null));
    }

    //survey
    @PostMapping("/verify/step3")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> verifyStep3(@Valid @RequestBody VerifyStep3Request body,
            @CurrentUser UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());

        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }
        User user = userOpt.get();
        UserProfile userProfile = user.getUserProfile();
        if (userProfile == null) {
            userProfile = new UserProfile();
            userProfile.setUser(user);
            userProfile.setPurposeOfAccount(body.getPurposeOfAccount());
            userProfile.setSourceFunds(body.getSourceFunds());
            userProfile.setMonthlyIncome(body.getMonthlyIncome());
            userProfile.setMonthlySpending(body.getMonthlySpending());
            userProfile.setOccupation(body.getOccupation());
            userProfile.setIndustry(body.getIndustry());
            userProfile.setPosition(body.getPosition());
            userProfile.setCompanyName(body.getCompanyName());
            userProfile.setCompanyAddress(body.getCompanyAddress());
            userProfile.setCompanyProvince(body.getCompanyProvince());
            userProfile.setCompanyCity(body.getCompanyCity());
            userProfile.setCompanyPhone(body.getCompanyPhone());
        }
        userProfile.setFillState(3);
        userProfileService.update(userProfile);
        return ResponseEntity.ok(new ApiResponse(true, null, "success verify step 3", null));
    }

    //Account RDN
    @PostMapping("/verify/step4")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> verifyStep4(@Valid @RequestBody VerifyStep4Request body,
            @CurrentUser UserPrincipal currentUser) {

        User user = userRepository.getOne(currentUser.getId());
        UserProfile userProfile = user.getUserProfile();
        if (userProfile == null) {
            user.setUserProfile(new UserProfile());
            userRepository.save(user);
        }
        
        Optional<Bank> optBank = bankRep.findById(body.getRdnbankId());
        if (!optBank.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Bank not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        userProfile.setRdnbankAccountHolder(body.getRdnbankAccountHolder());
        userProfile.setRdnbankAccountNo(body.getRdnbankAccountNo());
        userProfile.setRdnbankId(body.getRdnbankId());
        userProfile.setFillState(4);
        userProfileService.update(userProfile);

        return ResponseEntity.ok(new ApiResponse(true, null, "success verify step 4", null));
    }

    //Agree T&C
    @PostMapping("/verify/final")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> verifyStep5(@CurrentUser UserPrincipal currentUser) {

        User user = userRepository.getOne(currentUser.getId());
        UserProfile userProfile = user.getUserProfile();
        if (userProfile == null) {
            user.setUserProfile(new UserProfile());
            userRepository.save(user);
        }
        user.setRealAccount(true);
        userRepository.save(user);
        return ResponseEntity.ok(new ApiResponse(true, null, "success verify final", null));
    }

    //Verify Phone Number
    @PostMapping("/verify/phone")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> verifyPhone(@Valid @RequestBody VerifyPhoneRequest body, @CurrentUser UserPrincipal currentUser) {
        User user = userRepository.getOne(currentUser.getId());
        UserProfile userProfile = user.getUserProfile();
        System.out.println("User Profile phone number:" + userProfile.getPhoneNo());
        System.out.println("Phone number dari request: " + body.getPhoneNumber());
        if(userProfile.getPhoneNo().equals(body.getPhoneNumber())) {
            user.setIsVerifiedPhone(true);
            userRepository.save(user);

            return ResponseEntity.ok(new ApiResponse(true, null, "success verify phone number", null));
        }

        return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "User / phone Number not found!", null),
                HttpStatus.NOT_FOUND);
    }
}
