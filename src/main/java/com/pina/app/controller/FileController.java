/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.model.User;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.repository.UserRep;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import com.pina.app.service.FileService;
import java.util.Optional;
import com.pina.app.payload.response.MediaResponse;

/**
 *
 * @author Hp
 */
@RestController
public class FileController {

    @Autowired
    private FileService fileService;

    @Autowired
    private UserRep userRepository;

    @GetMapping("/api/media")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getMedia(@CurrentUser UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        User user = userOpt.get();
        MediaResponse res = new MediaResponse();
        res.setImageKtpUrl(user.getUserProfile().getImgKtpUrl());
        res.setImageKtpFaceUrl(user.getUserProfile().getImgFaceUrl());
        res.setImageNpwpUrl(user.getUserProfile().getImgNpwpUrl());
        res.setImageSignatureUrl(user.getUserProfile().getImgSignatureUrl());
        return ResponseEntity.ok(new ApiResponse(true, null, "Media fetched", res));
    }

    @GetMapping("/images/nf/{fileName:.+}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<byte[]> getImage(@PathVariable String fileName, HttpServletRequest request) {
        return fileService.getImage(fileName, "images/nf");
    }

    @GetMapping("/files/nf/{fileName:.+}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getFile(@PathVariable String fileName, HttpServletRequest request) {
        return fileService.getFile(fileName, "files/nf", request);
    }

    @PostMapping("/api/upload/avatar")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> uploadAvatar(@RequestParam("file") MultipartFile file, @CurrentUser UserPrincipal currentUser) {
        return fileService.uploadAvatar(currentUser, file, "avatar");
    }

    @PostMapping("/api/upload/stock")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> uploadStock(@RequestParam("file") MultipartFile file, @CurrentUser UserPrincipal currentUser) {
        return fileService.upload(file, "stock", currentUser);
    }

    @PostMapping("/api/upload/ktp")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> uploadKTP(@RequestParam("file") MultipartFile file, @CurrentUser UserPrincipal currentUser) {
        return fileService.upload(file, "ktp", currentUser);
    }

    @PostMapping("/api/upload/signature")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> uploadSignature(@RequestParam("file") MultipartFile file, @CurrentUser UserPrincipal currentUser) {
        return fileService.upload(file, "signature", currentUser);
    }

    @PostMapping("/api/upload/selfie")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> uploadSelfie(@RequestParam("file") MultipartFile file, @CurrentUser UserPrincipal currentUser) {
        return fileService.upload(file, "selfie", currentUser);
    }

    @PostMapping("/api/upload/npwp")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> uploadNPWP(@RequestParam("file") MultipartFile file, @CurrentUser UserPrincipal currentUser) {
        return fileService.upload(file, "npwp", currentUser);

    }

    @GetMapping("/images/stock/{fileName:.+}")
    public ResponseEntity<byte[]> getImageStock(@PathVariable String fileName, HttpServletRequest request) {
        return fileService.getImage(fileName, "stock");
    }

    @GetMapping("/images/ktp/{fileName:.+}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<byte[]> getImageKtp(@PathVariable String fileName, HttpServletRequest request) {
        return fileService.getImage(fileName, "ktp");
    }

    @GetMapping("/images/signature/{fileName:.+}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<byte[]> getImageSignature(@PathVariable String fileName, HttpServletRequest request) {
        return fileService.getImage(fileName, "signature");
    }

    @GetMapping("/images/selfie/{fileName:.+}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<byte[]> getImageSelfie(@PathVariable String fileName, HttpServletRequest request) {
        return fileService.getImage(fileName, "selfie");
    }

    @GetMapping("/images/npwp/{fileName:.+}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<byte[]> getImageNpwp(@PathVariable String fileName, HttpServletRequest request) {
        return fileService.getImage(fileName, "npwp");
    }

    @GetMapping(value = "/images/avatar/{fileName:.+}")
    public ResponseEntity<byte[]> getImageAvatar(@PathVariable String fileName, HttpServletRequest request) {
        return fileService.getImage(fileName, "avatar");
    }
}
