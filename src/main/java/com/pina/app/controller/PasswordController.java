/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.model.User;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.request.ResetLoginRequest;
import com.pina.app.payload.response.ResetVerifyRequest;
import com.pina.app.service.EmailService;
import com.pina.app.service.UserService;
import com.pina.app.util.StringGenerator;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Hp
 */
@RestController
public class PasswordController {

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @RequestMapping(value = "/api/forgot", method = RequestMethod.POST)
    public ResponseEntity<?> forgotPassword(@RequestBody Map<String, String> json) {
        String email = json.get("email");
        Optional<User> optional = userService.findUserByEmail(email);
        if (!optional.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(),
                    "We didn't find an account for that e-mail address", null),
                    HttpStatus.BAD_REQUEST);
        } else {

            User user = optional.get();
            user.setResetPasswordToken(UUID.randomUUID().toString());
            user.setResetPasswordSentAt(new Date());
            user.setVerificationCode(new String(StringGenerator.OTP(6)));

            // Save token to database
            userService.save(user);

            //String appUrl = request.getScheme() + "://" + request.getServerName();
            // Email message
            SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
            passwordResetEmail.setFrom("anwartubagus@gmail.com");
            passwordResetEmail.setTo(user.getEmail());
            passwordResetEmail.setSubject("Password Reset Code Verification");
//            passwordResetEmail.setText("To reset your password, click the link below:\n" + appUrl
//                    + "/reset?token=" + user.getResetPasswordToken());

            passwordResetEmail.setText("Code Verification : " + user.getVerificationCode());
            emailService.sendEmail(passwordResetEmail);

        }

        return ResponseEntity.ok(new ApiResponse(true, null, "request reset password success, check your email : " + email, null));
    }

    // @RequestMapping(value = "/api/verify", method = RequestMethod.POST)
    // public ResponseEntity<?> verify(@Valid @RequestBody ResetVerifyRequest body) {
    //     Optional<User> user = userService.findUserByVerificationCodeAndEmail(body.getCode(), body.getEmail());
    //     if (!user.isPresent()) {
    //         return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(),
    //                 "Oops!  This is an invalid code verification", null),
    //                 HttpStatus.BAD_REQUEST);
    //     }
    //     return ResponseEntity.ok(new ApiResponse(true, null, "verify valid", null));
    // }

    @RequestMapping(value = "/api/reset", method = RequestMethod.POST)
    public ResponseEntity<?> resetPassword(@Valid @RequestBody ResetLoginRequest body) {
        Optional<User> user = userService.findUserByVerificationCodeAndEmail(body.getCode(), body.getEmail());
        if (user.isPresent()) {
            User resetUser = user.get();
            resetUser.setPassword(bCryptPasswordEncoder.encode(body.getPassword()));
            resetUser.setResetPasswordToken(null);
            resetUser.setResetPasswordSentAt(null);
            resetUser.setVerificationCode(null);

            userService.save(resetUser);

            // Email message
            SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
            passwordResetEmail.setFrom("anwartubagus@gmail.com");
            passwordResetEmail.setTo(resetUser.getEmail());
            passwordResetEmail.setSubject("Change Password succes");
            passwordResetEmail.setText("Change Password succes");
            emailService.sendEmail(passwordResetEmail);

        } else {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(),
                    "Oops!  This is an invalid code verification", null),
                    HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(new ApiResponse(true, null, "reset password success", null));
    }

}
