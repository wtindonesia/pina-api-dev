package com.pina.app.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.model.Stock;
import com.pina.app.payload.request.StockRequest;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.Group;
import com.pina.app.payload.response.PagedResponse;
import com.pina.app.payload.response.StockResponse;
import com.pina.app.payload.response.StockIndexResponse;
import com.pina.app.service.StockService;
import com.pina.app.repository.StockRep;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/stocks")
public class StockController {

    @Autowired
    StockService stockService;

    @Autowired
    private StockRep stockRepository;

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getStockList(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "code", defaultValue = "") String code,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @RequestParam(value = "sort", defaultValue = "asc") String sort,
            @RequestParam(value = "sort_by", defaultValue = "code") String sortby,
            @CurrentUser UserPrincipal currentUser
    ) {
        Page<Stock> ls = null;

        if(sort.toLowerCase().equals("rand")) {
            ls = stockRepository.findAllRandom(new PageRequest(page, size));
        } else {
            Sort.Direction sortDirection = Sort.Direction.ASC;
            if (sort.toLowerCase().equals("asc")) {
                sortDirection = Sort.Direction.ASC;
            } else {
                sortDirection = Sort.Direction.DESC;
            }
            Pageable pageable = PageRequest.of(page, size, sortDirection, sortby);

            List<String> listCode = new ArrayList<>();
            if (!code.equals("")) {
                String[] codes = code.split("\\,");
                listCode.addAll(Arrays.asList(codes));
                ls = stockRepository.findAllByCodes(pageable, listCode);
            } else {
                ls = stockRepository.findAll(pageable);
            }
        }

        List<StockResponse> resList = ls.stream().map(stock -> {
            StockResponse stockRes = new StockResponse();
            stockRes.setCode(stock.getCode());
            stockRes.setName(stock.getName());
            stockRes.setLogoUrl(stock.getLogoUrl());
            stockRes.setChg(10);
            stockRes.setChgPct(0.45);
            stockRes.setLast(1000);
            return stockRes;
        }).collect(Collectors.toList());

        PagedResponse<StockResponse> result = new PagedResponse<>(resList, ls.getNumber(),
                ls.getSize(), ls.getTotalElements(), ls.getTotalPages(), ls.isLast());
        return ResponseEntity.created(null).body(new ApiResponse(true, null, "", result));
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getStock(@PathVariable("id") long id) {
        Stock stock = stockService.findById(id);
        if (stock == null) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "stock not found!", null),
                    HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(new ApiResponse(true, null, "", stock));
    }

    @PostMapping
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createStock(@Valid @RequestBody StockRequest stockRequest) {
        Stock stock = new Stock();
        stock.setCode(stockRequest.getCode());
        stock.setName(stockRequest.getName());
        stock.setLogoUrl(stockRequest.getLogoUrl());
        stockService.saveStock(stock);
        return ResponseEntity.created(null).body(new ApiResponse(true, null, "Stock Created Successfully", null));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> deteleStock(@PathVariable("id") long id) {

        Stock stock = stockService.findById(id);
        if (stock == null) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "id not found!", null),
                    HttpStatus.NOT_FOUND);
        }
        stockService.deleteStockById(id);
        return ResponseEntity.ok(new ApiResponse(true, null, "Stock delete Successfully", null));
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateUser(@PathVariable("id") long id, @RequestBody Stock user) {

        Stock currentUser = stockService.findById(id);

        if (currentUser == null) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.NOT_FOUND.asText(), "Stock id not found!", null),
                    HttpStatus.NOT_FOUND);
        }

        currentUser.setName(user.getName());
        currentUser.setCode(user.getCode());
        currentUser.setLogoUrl(user.getLogoUrl());

        stockService.updateStock(currentUser);
        return ResponseEntity.ok(new ApiResponse(true, null, "Stock update Successfully", null));
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ResponseEntity<?> getStocksIndex() {

        Stock aali = stockRepository.findByCode("AALI");
        Stock adro = stockRepository.findByCode("ADRO");

        StockResponse sAALI = new StockResponse();
        sAALI.setCode("AALI");
        sAALI.setName(aali.getName());
        sAALI.setLogoUrl(aali.getLogoUrl());
        sAALI.setLast(13600);
        sAALI.setChg(-100);
        sAALI.setChgPct(-0.72);

        StockResponse sADRO = new StockResponse();
        sADRO.setCode("ADRO");
        sADRO.setName(adro.getName());
        sADRO.setLogoUrl(adro.getLogoUrl());
        sADRO.setLast(2120);
        sADRO.setChg(10);
        sADRO.setChgPct(0.42);

        StockIndexResponse res = new StockIndexResponse();
        List<StockResponse> stocks = new ArrayList<>();
        stocks.add(sAALI);
        stocks.add(sADRO);

        List<Group> groups = new ArrayList<>();
        Group group = new Group();
        group.setChgPct(0);
        group.setName("topGainers");
        group.setTitle("Top Gainers In PINA");
        group.setStocks(stocks);
        groups.add(group);

        group = new Group();
        group.setChgPct(0);
        group.setName("topLosers");
        group.setTitle("Top Losers In PINA");
        group.setStocks(stocks);
        groups.add(group);

        group = new Group();
        group.setChgPct(0);
        group.setName("topFrequency");
        group.setTitle("Top Frequency In PINA");
        group.setStocks(stocks);
        groups.add(group);

        group = new Group();
        group.setChgPct(0);
        group.setName("topVolume");
        group.setTitle("Top Volume In PINA");
        group.setStocks(stocks);
        groups.add(group);

        group = new Group();
        group.setChgPct(0);
        group.setName("trendingNow");
        group.setTitle("Trending Now In PINA");
        group.setStocks(stocks);
        groups.add(group);

        group = new Group();
        group.setChgPct(0);
        group.setName("popular");
        group.setTitle("Popular In PINA");
        group.setStocks(stocks);
        groups.add(group);

        group = new Group();
        group.setChgPct(3.12f);
        group.setName("banking");
        group.setTitle("Banking");
        group.setStocks(stocks);
        groups.add(group);

        group = new Group();
        group.setChgPct(3.12f);
        group.setName("mining");
        group.setTitle("Mining");
        group.setStocks(stocks);
        groups.add(group);

        res.setGroups(groups);

        return ResponseEntity.created(null).body(new ApiResponse(true, null, "", res));
    }
}
