/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.payload.request.FollowStockRequest;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.request.FollowUserRequest;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import com.pina.app.service.UserService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Hp
 */
@RestController
@RequestMapping("/api/unfollow")
public class UnFollowController {

    @Autowired
    private UserService userService;

    @PostMapping("/users")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> unfollowUser(@Valid @RequestBody FollowUserRequest followUserRequest, @CurrentUser UserPrincipal currentUser
    ) {
        userService.unfollowUser(currentUser.getId(), followUserRequest.getUserIds());
        return ResponseEntity.ok(new ApiResponse(true, null, "Success unfollow users", null));
    }

    @PostMapping("/stocks")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> unfollowStock(@Valid @RequestBody FollowStockRequest followStockRequest, @CurrentUser UserPrincipal currentUser
    ) {
        userService.unfollowStock(currentUser.getId(), followStockRequest.getStockIds());
        return ResponseEntity.ok(new ApiResponse(true, null, "Success unfollow stocks", null));
    }
}
