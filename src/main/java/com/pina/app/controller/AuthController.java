package com.pina.app.controller;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.exception.AppException;
import com.pina.app.model.FacebookSignup;
import com.pina.app.model.Role;
import com.pina.app.model.RoleName;
import com.pina.app.model.User;
import com.pina.app.model.UserProfile;
import com.pina.app.payload.request.FacebookSignupRequest;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.JwtAuthenticationResponse;
import com.pina.app.payload.request.LoginRequest;
import com.pina.app.payload.request.PinCheckRequest;
import com.pina.app.payload.request.SignUpRequest;
import com.pina.app.payload.response.CheckTokenResponse;
import com.pina.app.payload.response.LoginFBRequest;
import com.pina.app.security.JwtTokenProvider;
import com.pina.app.util.DateFormatUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import com.pina.app.repository.RoleRep;
import com.pina.app.repository.UserRep;
import com.pina.app.repository.UserProfileRep;
import com.pina.app.service.EmailService;
import com.pina.app.service.RealTradingService;
import java.util.Arrays;
import org.springframework.core.env.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;


import org.springframework.kafka.core.KafkaTemplate;
/**
 * Created by hp on 02/08/17.
 */
@RestController
@RequestMapping("/api")
public class AuthController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private Environment env;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRep userRepository;

    @Autowired
    RoleRep roleRepository;

    @Autowired
    UserProfileRep userProfileRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    private EmailService emailService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    RealTradingService realTradingService;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @PostMapping("/auth/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest body) {
        Optional<User> optUser = userRepository.findByUserNameOrEmail(body.getEmail(), body.getEmail());
        if (optUser.isPresent()) {
            if (body.getPassword().equals("fbtoken")) {
                return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Email/Username or Password not valid!", null),
                        HttpStatus.BAD_REQUEST);
            }

            boolean exist = passwordEncoder.matches(body.getPassword(), optUser.get().getPassword());
            if (!exist) {
                return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Email/Username or Password not valid!", null),
                        HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Email/Username or Password not valid!", null),
                    HttpStatus.BAD_REQUEST);
        }

        User user = optUser.get();
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        body.getEmail(),
                        body.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        JwtAuthenticationResponse res = new JwtAuthenticationResponse(jwt);
        if (user.getLastLogin() != null) {
            res.setLastLogin(DateFormatUtil.format(user.getLastLogin()));
        }
        user.setLastLogin(new Date());
        userRepository.save(user);

        /**
         * State for verify and starter
         * @author      Sandi Andrian<andrian.sandi@gmail.com>
         * @since       Nov 4, 2019
         **/
        res.setFillState(user.getUserProfile().getFillState());
        res.setStarterState(user.getUserProfile().getStarterState());

        return ResponseEntity.ok(new ApiResponse(true, null, "token generated", res));
    }

    @PostMapping("/auth/signin/fb")
    public ResponseEntity<?> authenticateUserByFb(@Valid @RequestBody LoginFBRequest body) {
        Optional<User> optUser = userRepository.findByEmail(body.getEmail());
        if (!optUser.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Email not registered!", null),
                    HttpStatus.BAD_REQUEST);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

        String token = body.getToken();
//        String accesToken = env.getProperty("facebook.access_token");
//        String json = restTemplate.exchange("https://graph.facebook.com/debug_token?input_token=" + token
//                + "&access_token=" + accesToken,
//                HttpMethod.GET, entity, String.class).getBody();
//
//        boolean valid = JsonPath.from(json).get("data.is_valid");
//        if (!valid) {
//            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Access token not valid or expired!", null),
//                    HttpStatus.BAD_REQUEST);
//
//        }

        User user = optUser.get();
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        body.getEmail(),
                        "fbtoken"
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        JwtAuthenticationResponse res = new JwtAuthenticationResponse(jwt);
        if (user.getLastLogin() != null) {
            res.setLastLogin(DateFormatUtil.format(user.getLastLogin()));
        }
        user.setLastLogin(new Date());
        userRepository.save(user);

        return ResponseEntity.ok(new ApiResponse(true, null, "token generated", res));
    }

    @PostMapping("/auth/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest body) {

        if (userRepository.existsByEmail(body.getEmail())) {
            logger.info("signup : email " + body.getEmail() + " already exist");
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Email Address already in use!", null),
                    HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByUserName(body.getUserName())) {
            logger.info("signup : username " + body.getUserName() + " already exist");
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Username already in use!", null),
                    HttpStatus.BAD_REQUEST);
        }

        logger.info("FULLNAME: " + body.getFullName());

        // Creating user's account
        User user = new User(body.getUserName(),
                body.getEmail(), body.getPassword());

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));

        User result = userRepository.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(result.getUserName()).toUri();

        logger.info("RESULT: " + result.getId());

        // Creating user's profile
        Optional<User> optUser = userRepository.findById(result.getId());
        User registeredUser = optUser.get();
        UserProfile userProfile = new UserProfile();
        userProfile.setUser(registeredUser);
        userProfile.setFullName(body.getFullName());
        userProfile.setStarterState(0);
        registeredUser.setUserProfile(userProfile);
        userProfileRepository.save(userProfile);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
        // TODO:
        // JSON stringify registeredUser atau userProfile
        // ke variable eventPayload
        // trus kirim :
        // kafkaTemplate.send("pina_user_signed_up", eventPayload );

        Map<String, String> map = new HashMap<String, String>();  
        map.put("email", body.getEmail());  
        map.put("userName", body.getUserName());  
        map.put("fullName", body.getFullName());  
        
        StringBuilder stringBuilder = new StringBuilder();

        for (String key : map.keySet()) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append("&");
            }
            String value = map.get(key);
            try {
                stringBuilder.append((key != null ? URLEncoder.encode(key, "UTF-8") : ""));
                stringBuilder.append("=");
                stringBuilder.append(value != null ? URLEncoder.encode(value, "UTF-8") : "");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("This method requires UTF-8 encoding support", e);
            }
        }

        String dataUser = stringBuilder.toString();;

        kafkaTemplate.send("pina_user_signed_up", dataUser);

        return ResponseEntity.created(location).body(new ApiResponse(true, null, "User registered successfully. Please login", null));
    }

    @PostMapping("/auth/signup/fb")
    public ResponseEntity<?> facebookSignup(@Valid @RequestBody FacebookSignupRequest body) {
        if (userRepository.existsByEmail(body.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Email Address already in use!", null),
                    HttpStatus.BAD_REQUEST);
        }

        String[] arrStr = body.getEmail().split("@");
        String username = arrStr[0];
        User user = new User(username,
                body.getEmail(), "fbtoken");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setFacebookSignup(new FacebookSignup(body.getId(), body.getEmail(), body.getName(), body.getPhoto()));

        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));
        userRepository.save(user);

        return ResponseEntity.created(null).body(new ApiResponse(true, null, "User registered by facebook successfully", null));
    }

    @GetMapping("/auth/checktoken/{token}")
    public ResponseEntity<?> checkToken(@PathVariable("token") String token) {
        Optional<User> userOpt = null;
        if (tokenProvider.validateToken(token)) {
            userOpt = userRepository.findById(tokenProvider.getUserIdFromJWT(token));
            if (!userOpt.isPresent()) {
                return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "User not found!", null),
                        HttpStatus.BAD_REQUEST);
            }
        }

        User user = userOpt.get();
        CheckTokenResponse res = new CheckTokenResponse();
        res.setEmail(user.getEmail());
        res.setUserName(user.getUserName());
        return ResponseEntity.ok(new ApiResponse(true, null, "success", res));
    }

    @GetMapping("/auth/checktokentr/{token}")
    public ResponseEntity<?> checkTokenTrading(@PathVariable("token") String token) {
        return realTradingService.checkToken(token);
    }
    
    @PostMapping("/auth/session")
    public ResponseEntity<?> getSession(@Valid @RequestBody PinCheckRequest body) {
        return realTradingService.checkSession(body);
    }
}
