/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.exception.BadRequestException;
import com.pina.app.model.QType;
import com.pina.app.model.Survey;
import com.pina.app.model.SurveyAnswer;
import com.pina.app.model.User;
import com.pina.app.payload.request.SurveyAddRequest;
import com.pina.app.payload.request.SurveyAnswerRequest;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.repository.SurveyAnswerRep;
import com.pina.app.repository.SurveyRep;
import java.beans.PropertyEditorSupport;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;

import com.pina.app.repository.UserRep;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Hp
 */
@RestController
@RequestMapping("/api")
public class SurveyController {

    @Autowired
    private SurveyRep surveyRep;

    @Autowired
    UserRep userRepository;

    @Autowired
    private SurveyAnswerRep surveyAnswerRep;

    @GetMapping("/survey/{label}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getSurvey(@PathVariable("label") String label) {
        List<Survey> ls = surveyRep.findAllByLabel(label);
        return ResponseEntity.ok(new ApiResponse(true, null, "", ls));
    }

    @PostMapping("/survey/answer")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> answerSurvey(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SurveyAnswerRequest body) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());

        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "User not Found", null),
                    HttpStatus.BAD_REQUEST);
        }

        User user = userOpt.get();

        SurveyAnswer answer = new SurveyAnswer();
        answer.setUser(user);
        answer.setAnswers(body.getAnswer());
        surveyAnswerRep.save(answer);

        return ResponseEntity.ok(new ApiResponse(true, null, "Survey Answer Recorded", null));
    }

    @PostMapping("/survey")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> createProvince(@Valid @RequestBody SurveyAddRequest body) {

        Survey survey = new Survey();
        survey.setLabel(body.getLabel());
        survey.setType(QType.fromValue(body.getType()));
        survey.setQuestion(body.getQuestion());
        survey.setOptions(body.getOptions());

        try {
            surveyRep.save(survey);
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("Duplicate Data");
        }

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(survey.getId()).toUri();
        return ResponseEntity.created(location).body(new ApiResponse(true, null, "Survey Created", null));
    }

    @InitBinder
    public void initBinder(final WebDataBinder webdataBinder) {
        webdataBinder.registerCustomEditor(QType.class, new SurveyTypeConverter());
    }

}

class SurveyTypeConverter extends PropertyEditorSupport {

    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
        setValue(QType.fromValue(text));
    }

}
