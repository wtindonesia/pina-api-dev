/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.controller;

import com.pina.app.exception.ResourceNotFoundException;
import com.pina.app.model.Stock;
import com.pina.app.model.User;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.FollowStockResponse;
import com.pina.app.payload.request.FollowStockRequest;
import com.pina.app.payload.request.FollowUserRequest;
import com.pina.app.payload.response.FollowUserResponse;
import com.pina.app.payload.response.PagedResponse;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import com.pina.app.service.UserService;
import com.pina.app.util.ModelMapper;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.pina.app.repository.StockRep;
import com.pina.app.repository.UserRep;
import java.util.List;

/**
 *
 * @author Hp
 */
@RestController
@RequestMapping("/api/follow")
public class FollowController {

    @Autowired
    private UserRep userRepository;

    @Autowired
    private StockRep stockRepository;

    @Autowired
    private UserService userService;

    @GetMapping("")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> findInfulencer(@RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size,
            @RequestParam(value = "sort", defaultValue = "asc") String sort,
            @RequestParam(value = "sort_by", defaultValue = "name") String sortby,
            @RequestParam(value = "by", defaultValue = "", required = true) String by,
            @CurrentUser UserPrincipal currentUser
    ) {
        System.out.println("Hello World!");
        Sort.Direction sortDirection = Sort.Direction.ASC;
        if (sort.toLowerCase().equals("asc")) {
            sortDirection = Sort.Direction.ASC;
        } else {
            sortDirection = Sort.Direction.DESC;
        }
        Pageable pageable = PageRequest.of(page, size, sortDirection, sortby);
        if (by.equals("user")) {
            User user = userRepository.findById(currentUser.getId())
                    .orElseThrow(() -> new ResourceNotFoundException("User", "id", currentUser.getId()));
            Page<User> ls = userRepository.findAllByInfluencer(pageable, true);
            List<FollowUserResponse> resList = ls.map(u -> {
                return ModelMapper.mapFollowUserResponse(user.getFollowUsers(), u);
            }).getContent();

            PagedResponse<FollowUserResponse> result = new PagedResponse<>(resList, ls.getNumber(),
                    ls.getSize(), ls.getTotalElements(), ls.getTotalPages(), ls.isLast());

            return ResponseEntity.ok(new ApiResponse(true, null, "", result));
        } else if (by.equals("stock")) {

            User user = userRepository.findById(currentUser.getId())
                    .orElseThrow(() -> new ResourceNotFoundException("User", "id", currentUser.getId()));
            Page<Stock> ls = stockRepository.findAll(pageable);
            List<FollowStockResponse> resList = ls.map(s -> {
                return ModelMapper.mapFollowStockResponse(user.getFollowStocks(), s);
            }).getContent();

            PagedResponse<FollowStockResponse> result = new PagedResponse<>(resList, ls.getNumber(),
                    ls.getSize(), ls.getTotalElements(), ls.getTotalPages(), ls.isLast());

            return ResponseEntity.ok(new ApiResponse(true, null, "", result));

        }
        return ResponseEntity.ok(new ApiResponse(true, null, "", null));
    }

    @GetMapping("/users")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getFollowUser(@CurrentUser UserPrincipal currentUser) {

        User user = userRepository.findById(currentUser.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", currentUser.getId()));
        return ResponseEntity.ok(new ApiResponse(true, null, "", ModelMapper.mapFollowUserResponse(user.getFollowUsers())));
    }

    @PostMapping("/users")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> followUser(@Valid @RequestBody FollowUserRequest followUserRequest, @CurrentUser UserPrincipal currentUser
    ) {
        userService.followUser(currentUser.getId(), followUserRequest.getUserIds());
        System.out.println("MAHO 3");
        return ResponseEntity.ok(new ApiResponse(true, null, "Success follow", null));
    }

    @GetMapping("/stocks")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getFollowStock(@CurrentUser UserPrincipal currentUser) {

        User user = userRepository.findById(currentUser.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", currentUser.getId()));
        System.out.println("MAHO 1");
        return ResponseEntity.ok(new ApiResponse(true, null, "", ModelMapper.mapFollowStockResponse(user.getFollowStocks())));
    }

    @PostMapping("/stocks")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> followStock(@Valid @RequestBody FollowStockRequest followStockRequest, @CurrentUser UserPrincipal currentUser
    ) {
        userService.followStock(currentUser.getId(), followStockRequest.getStockIds());
        System.out.println("MAHO 2");
        return ResponseEntity.ok(new ApiResponse(true, null, "Success follow", null));
    }
}
