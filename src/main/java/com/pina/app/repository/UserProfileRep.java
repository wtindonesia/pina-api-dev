/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.repository;

import com.pina.app.model.UserProfile;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Hp
 */
public interface UserProfileRep extends JpaRepository<UserProfile, Long> {

    @Override
    Optional<UserProfile> findById(Long userId);
}
