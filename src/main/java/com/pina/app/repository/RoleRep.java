package com.pina.app.repository;

import com.pina.app.model.Role;
import com.pina.app.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by hp on 02/08/17.
 */
@Repository
public interface RoleRep extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
