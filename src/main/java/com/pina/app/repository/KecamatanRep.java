/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.repository;

import com.pina.app.model.Kecamatan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author Hp
 * @updated    Nov 22, 2019
 */
@Repository
public interface KecamatanRep extends JpaRepository<Kecamatan, Integer> {

    Kecamatan findByName(String name);

    List<Kecamatan> findKecamatanByCityId(int cityId);
}
