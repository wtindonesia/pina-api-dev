package com.pina.app.repository;

import org.springframework.stereotype.Repository;

import com.pina.app.model.Stock;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface StockRep extends JpaRepository<Stock, Long> {

    @Override
    Optional<Stock> findById(Long stockId);

    List<Stock> findByIdIn(List<Long> stockIds);

    List<Stock> findByCodeIn(List<String> stockCodes);

    Stock findByCode(String code);

    Stock findByName(String name);

    //use native query
    @Query(
            value = "SELECT * FROM stocks where code in(:x) ORDER BY id",
            countQuery = "SELECT count(*) FROM stocks",
            nativeQuery = true)
    Page<Stock> findAllByCodes(Pageable pageable, @Param("x") List<String> codes);

    /**
     * Get Random Stocks
     *
     * @author     Sandi Andrian <andrian.sandi@gmail.com>
     * @since      Nov 14, 2019
     **/
    @Query(value = "select * from stocks order by RANDOM()",
            countQuery = "select count(*) from stocks",
            nativeQuery = true)
    Page<Stock> findAllRandom(Pageable pageable);

}
