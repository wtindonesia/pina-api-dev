/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.repository;

import com.pina.app.model.NewsFeed;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Hp
 */
public interface NewsFeedRep extends JpaRepository<NewsFeed, Long> {
    //use native query
    @Query(
            value = "select * from news_feeds t1 left join follow_users t2 "
                    + "on t1.user_id=t2.follow_user_id "
                    + "where t2.user_id=:userId or t1.user_id=:userId ORDER BY created_at desc",
            countQuery = "SELECT count(*) FROM news_feeds",
            nativeQuery = true)
    Page<NewsFeed> findAllLatest(Pageable pageable,@Param("userId") Long userId);
    
    
}
