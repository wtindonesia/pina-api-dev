/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.repository;

import com.pina.app.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author Hp
 */
@Repository
public interface CityRep extends JpaRepository<City, Integer> {

    City findByName(String name);

    List<City> findAllByProvinceId(int province_id);
}
