/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.repository;

import com.pina.app.model.FAQ;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Hp
 */
public interface FAQRep extends JpaRepository<FAQ, Integer> {

    List<FAQ> findAllByName(String name);
}
