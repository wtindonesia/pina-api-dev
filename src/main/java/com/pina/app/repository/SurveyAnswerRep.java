/**
 *
 * @author  Sandi Andrian <andrian.sandi@gmail.com>
 * @since   Nov 19, 2019
 **/
package com.pina.app.repository;

import com.pina.app.model.SurveyAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SurveyAnswerRep extends JpaRepository<SurveyAnswer, Integer> {
}
