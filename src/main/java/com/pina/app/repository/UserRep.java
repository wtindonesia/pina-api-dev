package com.pina.app.repository;

import com.pina.app.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by hp
 */
@Repository
public interface UserRep extends JpaRepository<User, Long> {

    @Override
    Optional<User> findById(Long userId);

    // will generate SELECT * FROM user WHERE email = ?
    Optional<User> findByEmail(String email);

    Optional<User> findByUserNameOrEmail(String username, String email);

    List<User> findByIdIn(List<Long> userIds);

    //normal JPA
    Page<User> findAllByInfluencer(Pageable pgbl, boolean influencer);

    //use JPQL
    @Query(value = "select u from User u where influencer=:x ORDER BY id")
    Page<User> findAllInfluencerJPQL(Pageable p, @Param("x") boolean x);

    //use native query
    @Query(
            value = "SELECT * FROM users where influencer=:x ORDER BY id",
            countQuery = "SELECT count(*) FROM users",
            nativeQuery = true)
    Page<User> findAllInfluencerNative(Pageable pageable, @Param("x") boolean x);

    //Get All influencer by Random
    @Query(
            value = "SELECT * FROM users where influencer=true ORDER BY RANDOM()",
            countQuery = "SELECT count(*) FROM users",
            nativeQuery = true)
    Page<User> findAllInfluencerRandom(Pageable pageable);

    Optional<User> findByUserName(String username);

    Boolean existsByUserName(String username);

    Boolean existsByEmail(String email);

    Optional<User> findByResetPasswordToken(String resetToken);

    Optional<User> findByVerificationCode(String code);

    Optional<User> findUserByVerificationCodeAndEmail(String code, String email);
}
