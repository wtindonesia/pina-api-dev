/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.repository;

import com.pina.app.model.Comment;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Hp
 */
public interface CommentRep extends JpaRepository<Comment, Long> {

    @Query(
            value = "SELECT * FROM comments t1 "
            + "inner join news_feed_comments t2 on t1.id = t2.comment_id "
            + "where news_feed_id=:x order by created_at desc limit 1",
            nativeQuery = true)
    Optional<Comment> GetOneByDateLatest(@Param("x") long newsFeedId);
}
