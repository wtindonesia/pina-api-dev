package com.pina.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import com.pina.app.model.DemoProfile;


@Repository
public interface DemoRepo extends JpaRepository<DemoProfile, String>{
    
    @Query(
        value = "SELECT * FROM demo where demo.user_id in(:x) ORDER BY updated_at", 
         countQuery = "SELECT count(*) FROM demo", nativeQuery = true
        )
    //Page<Stock> findAllByCodes(Pageable pageable, @Param("x") List<String> codes);
    List<DemoProfile> findByUserId(@Param("x") Long id);

    @Query(
        value = "SELECT * FROM demo where demo.key in(:x) ORDER BY updated_at", 
        countQuery = "SELECT count(*) FROM demo", 
        nativeQuery = true)
	List<DemoProfile> findByKey(@Param("x") String key);

   
}