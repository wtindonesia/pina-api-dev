/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.repository;

import com.pina.app.model.Bank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Hp
 */
@Repository
public interface BankRep extends JpaRepository<Bank, Integer> {
    Bank findByName(String name);
}
