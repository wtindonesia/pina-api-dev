/**
 * Verify Phone Request
 *
 * @author  Sandi Andrian <andrian.sandi@gmail.com>
 * @since   Nov 26, 2019
 **/
package com.pina.app.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class VerifyPhoneRequest {

    @NotBlank
    @Size(max = 200)
    private String phoneNumber;

    public String getPhoneNumber() { return phoneNumber; }

    public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }
}
