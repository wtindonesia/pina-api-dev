/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Hp
 */
public class VerifyStep4Request {

    @NotNull
    private Integer rdnbankId;
    @Size(max = 200)
    private String rdnbankAccountNo;
    @Size(max = 200)
    private String rdnbankAccountHolder;

    public Integer getRdnbankId() {
        return rdnbankId;
    }

    public void setRdnbankId(Integer rdnbankId) {
        this.rdnbankId = rdnbankId;
    }

    public String getRdnbankAccountNo() {
        return rdnbankAccountNo;
    }

    public void setRdnbankAccountNo(String rdnbankAccountNo) {
        this.rdnbankAccountNo = rdnbankAccountNo;
    }

    public String getRdnbankAccountHolder() {
        return rdnbankAccountHolder;
    }

    public void setRdnbankAccountHolder(String rdnbankAccountHolder) {
        this.rdnbankAccountHolder = rdnbankAccountHolder;
    }

}
