/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.request;

/**
 *
 * @author Hp
 */
public class NewsFeedCreateRequest {

    private Long shareFromId;
    private String content;
    private String access;
    private String type;
    private String photoURLs;
    private String attchmentURLs;

    public Long getShareFromId() {
        return shareFromId;
    }

    public void setShareFromId(Long shareFromId) {
        this.shareFromId = shareFromId;
    }
    
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttchmentURLs() {
        return attchmentURLs;
    }

    public void setAttchmentURLs(String attchmentURLs) {
        this.attchmentURLs = attchmentURLs;
    }

    public String getPhotoURLs() {
        return photoURLs;
    }

    public void setPhotoURLs(String photoURLs) {
        this.photoURLs = photoURLs;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    

}
