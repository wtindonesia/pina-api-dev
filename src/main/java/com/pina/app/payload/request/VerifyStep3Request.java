/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.request;

import com.pina.app.model.Question;
import java.util.List;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Hp
 */
public class VerifyStep3Request {

    private String purposeOfAccount;
    private String sourceFunds;
    private String monthlyIncome;
    private String monthlySpending;
    private String occupation;
    private String industry;
    private String position;
    private String companyName;
    private String companyAddress;
    private int companyProvince;
    private int companyCity;
    private String companyPhone;

    public String getPurposeOfAccount() { return purposeOfAccount; }
    public void setPurposeOfAccount(String purposeOfAccount) { this.purposeOfAccount = purposeOfAccount; }

    public String getSourceFunds() { return sourceFunds; }
    public void setSourceFunds(String sourceFunds) { this.sourceFunds = sourceFunds; }

    public String getMonthlyIncome() { return monthlyIncome; }
    public void setMonthlyIncome(String monthlyIncome) { this.monthlyIncome = monthlyIncome; }

    public String getMonthlySpending() { return monthlySpending; }
    public void setMonthlySpending(String monthlySpending) { this.monthlySpending = monthlySpending; }

    public String getOccupation() { return occupation; }
    public void setOccupation(String occupation) { this.occupation = occupation; }

    public String getIndustry() { return industry; }
    public void setIndustry(String industry) { this.industry = industry; }

    public String getPosition() { return position; }
    public void setPosition(String position) { this.position = position; }

    public String getCompanyName() { return companyName; }
    public void setCompanyName(String companyName) { this.companyName = companyName; }

    public String getCompanyAddress() { return companyAddress; }
    public void setCompanyAddress(String companyAddress) { this.companyAddress = companyAddress; }

    public int getCompanyProvince() { return companyProvince; }
    public void setCompanyProvince(int companyProvince) { this.companyProvince = companyProvince; }

    public int getCompanyCity() { return companyCity; }
    public void setCompanyCity(int companyCity) { this.companyCity = companyCity; }

    public String getCompanyPhone() { return companyPhone; }
    public void setCompanyPhone(String companyPhone) { this.companyPhone = companyPhone; }
}
