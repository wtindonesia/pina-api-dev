/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.request.master;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Hp
 */
public class KelurahanRequest {

    @NotBlank
    @Size(max = 255)
    private String name;

    @NotNull
     @Min(1)
    private Integer kecamatanId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getKecamatanId() {
        return kecamatanId;
    }

    public void setKecamatanId(Integer kecamatanId) {
        this.kecamatanId = kecamatanId;
    }
    
}
