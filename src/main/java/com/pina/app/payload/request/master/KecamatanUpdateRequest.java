/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.request.master;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 *
 * @author Hp
 */
public class KecamatanUpdateRequest {

    @Size(max = 255)
    private String name;
    @Min(1)
    private Integer cityId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

}
