/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.request;

import javax.validation.constraints.Size;

/**
 *
 * @author Hp
 */
public class VerifyStep1Request {

    @Size(max = 200)
    private String imgKtpUrl;
    @Size(max = 200)
    private String imgFaceUrl;
    @Size(max = 200)
    private String imgSignatureUrl;
    @Size(max = 200)
    private String imgNpwpUrl;

    public String getImgKtpUrl() {
        return imgKtpUrl;
    }

    public void setImgKtpUrl(String imgKtpUrl) {
        this.imgKtpUrl = imgKtpUrl;
    }

    public String getImgFaceUrl() {
        return imgFaceUrl;
    }

    public void setImgFaceUrl(String imgFaceUrl) {
        this.imgFaceUrl = imgFaceUrl;
    }

    public String getImgSignatureUrl() {
        return imgSignatureUrl;
    }

    public void setImgSignatureUrl(String imgSignatureUrl) {
        this.imgSignatureUrl = imgSignatureUrl;
    }

    public String getImgNpwpUrl() {
        return imgNpwpUrl;
    }

    public void setImgNpwpUrl(String imgNpwpUrl) {
        this.imgNpwpUrl = imgNpwpUrl;
    }
    
    

}
