/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.request;

import java.util.List;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Hp
 */
public class FollowStockRequest {

    @NotNull
    private List<Long> stockIds;

	public List<Long> getStockIds() {
		return stockIds;
	}

	public void setStockIds(List<Long> stockIds) {
		this.stockIds = stockIds;
	}

  
}
