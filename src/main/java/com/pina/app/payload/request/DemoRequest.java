package com.pina.app.payload.request;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.*;
import javax.validation.constraints.NotBlank;


public class DemoRequest {
    
    @NotBlank
    @Size(max = 100)
    private String key;

    //@Type(type = "jsonb")
    private Map<String, Object> value;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Map<String, Object> getValue() {
        return value;
    }

    public void setValue(Map<String, Object> value) {
        this.value = value;
    }

}