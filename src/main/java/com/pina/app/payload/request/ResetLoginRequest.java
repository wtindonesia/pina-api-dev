package com.pina.app.payload.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * Created by hp on 02/08/17.
 */
public class ResetLoginRequest {

    @NotBlank
    private String password;

    @NotBlank
    private String code;
    
    @NotBlank
    @Email
    private String email;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
    
}
