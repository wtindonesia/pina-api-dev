/**
 *
 * @author  Sandi Andrian <andrian.sandi@gmail.com>
 * @since   Nov 19, 2019
 **/

package com.pina.app.payload.request;

import com.pina.app.model.SurveyAnswerChoice;
import java.util.List;

public class SurveyAnswerRequest {
    public List<SurveyAnswerChoice> answer;

    public List<SurveyAnswerChoice> getAnswer() {
        return answer;
    }

    public void setAnswer(List<SurveyAnswerChoice> answer) {
        this.answer = answer;
    }
}
