/**
 * Request for Starter
 *
 * @author  Sandi Andrian (andrian.sandi@gmail.com)
 * @since   Nov 4, 2019
 **/

package com.pina.app.payload.request;

import javax.validation.constraints.NotBlank;

public class StarterRequest {
    @NotBlank
    private Integer starterState;

    public Integer getStarterState() { return starterState; }

    public void setStarterState(Integer starterState) { this.starterState = starterState; }
}
