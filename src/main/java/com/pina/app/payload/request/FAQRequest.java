/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.request;

import com.pina.app.model.FAQItem;
import java.util.List;

/**
 *
 * @author Hp
 */
public class FAQRequest {
    private String name;
    private List<FAQItem> content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FAQItem> getContent() {
        return content;
    }

    public void setContent(List<FAQItem> content) {
        this.content = content;
    }
    
    
}
