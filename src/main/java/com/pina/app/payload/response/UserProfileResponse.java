/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.response;

/**
 *
 * @author Hp
 */
public class UserProfileResponse {

    private Long userId;
    private String fullName;
    private String userName;
    private String imgAvatarUrl;
    private String email;
    private boolean realAccount;
    private boolean realAccountActive;
    private boolean pinCreated;
    private boolean influencer;
    private String lastLogin;
    private int fillState;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImgAvatarUrl() {
        return imgAvatarUrl;
    }

    public void setImgAvatarUrl(String imgAvatarUrl) {
        this.imgAvatarUrl = imgAvatarUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public boolean isRealAccount() {
        return realAccount;
    }

    public void setRealAccount(boolean realAccount) {
        this.realAccount = realAccount;
    }

    public boolean isRealAccountActive() {
        return realAccountActive;
    }

    public void setRealAccountActive(boolean realAccountActive) {
        this.realAccountActive = realAccountActive;
    }

    public boolean isPinCreated() {
        return pinCreated;
    }

    public void setPinCreated(boolean pinCreated) {
        this.pinCreated = pinCreated;
    }

    public boolean isInfluencer() {
        return influencer;
    }

    public void setInfluencer(boolean influencer) {
        this.influencer = influencer;
    }
    
    

    public int getFillState() {
        return fillState;
    }

    public void setFillState(int fillState) {
        this.fillState = fillState;
    }

}
