/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.response.trading;

import com.pina.app.model.Stock;
import java.util.List;

/**
 *
 * @author Hp
 */
public class PortfolioResponse {

    private double value;
    private double profit;
    private double profitPct;
    private double balance;
    private double availableCash;
    private PortfolioPct portfolioPct;
    private List<Stock> stockRecommendation;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public double getProfitPct() {
        return profitPct;
    }

    public void setProfitPct(double profitPct) {
        this.profitPct = profitPct;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getAvailableCash() {
        return availableCash;
    }

    public void setAvailableCash(double availableCash) {
        this.availableCash = availableCash;
    }

    public PortfolioPct getPortfolioPct() {
        return portfolioPct;
    }

    public void setPortfolioPct(PortfolioPct portfolioPct) {
        this.portfolioPct = portfolioPct;
    }

    public List<Stock> getStockRecommendation() {
        return stockRecommendation;
    }

    public void setStockRecommendation(List<Stock> stockRecommendation) {
        this.stockRecommendation = stockRecommendation;
    }

    

    
    
}
