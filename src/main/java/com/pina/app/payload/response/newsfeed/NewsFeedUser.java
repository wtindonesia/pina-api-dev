/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.response.newsfeed;

/**
 *
 * @author Hp
 */
public class NewsFeedUser {
    private Long id;
    private Boolean influencer;
    private String ImgAvatarUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getInfluencer() {
        return influencer;
    }

    public void setInfluencer(Boolean influencer) {
        this.influencer = influencer;
    }

    public String getImgAvatarUrl() {
        return ImgAvatarUrl;
    }

    public void setImgAvatarUrl(String ImgAvatarUrl) {
        this.ImgAvatarUrl = ImgAvatarUrl;
    }
    
    
    
}
