/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.response.newsfeed;

/**
 *
 * @author Hp
 */
public class NFUser {
    private long id;
    private String name;
    private Boolean influencer;
    private String ImgAvatarUrl;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getInfluencer() {
        return influencer;
    }

    public void setInfluencer(Boolean influencer) {
        this.influencer = influencer;
    }

    public String getImgAvatarUrl() {
        return ImgAvatarUrl;
    }

    public void setImgAvatarUrl(String ImgAvatarUrl) {
        this.ImgAvatarUrl = ImgAvatarUrl;
    }
    
    
    
}