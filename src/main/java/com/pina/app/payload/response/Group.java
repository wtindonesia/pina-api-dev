/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.response;

import java.util.List;

/**
 *
 * @author Hp
 */
public class Group {

    private String name;
    private String title;
    private float chgPct;
    private List<StockResponse> stocks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    
    
    
    
    public float getChgPct() {
        return chgPct;
    }

    public void setChgPct(float chgPct) {
        this.chgPct = chgPct;
    }

    public List<StockResponse> getStocks() {
        return stocks;
    }

    public void setStocks(List<StockResponse> stocks) {
        this.stocks = stocks;
    }

}
