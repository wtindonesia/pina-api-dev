/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.response.trading;

import java.util.List;

/**
 *
 * @author Hp
 */
public class DepositResponse {

    private double cashAvailable;
    private String bankName;
    private String bankAccountNo;
    private String bankAccountName;
    private String bankLogoUrl;
    List<Deposit> depositHistory;

    public double getCashAvailable() {
        return cashAvailable;
    }

    public void setCashAvailable(double cashAvailable) {
        this.cashAvailable = cashAvailable;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankLogoUrl() {
        return bankLogoUrl;
    }

    public void setBankLogoUrl(String bankLogoUrl) {
        this.bankLogoUrl = bankLogoUrl;
    }

    
    
    public List<Deposit> getDepositHistory() {
        return depositHistory;
    }

    public void setDepositHistory(List<Deposit> depositHistory) {
        this.depositHistory = depositHistory;
    }

}
