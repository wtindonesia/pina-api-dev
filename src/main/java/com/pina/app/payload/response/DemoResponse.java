package com.pina.app.payload.response;

import java.util.Map;

import javax.validation.constraints.*;
import javax.validation.constraints.NotBlank;

public class DemoResponse {
    @NotBlank
    @Size(max = 100)
    private String key;

    // @Type(type = "jsonb")
    private Map<String, Object> value;

    private Long userId;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Map<String, Object> getValue() {
        return value;
    }

    public void setValue(Map<String, Object> value) {
        this.value = value;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

}