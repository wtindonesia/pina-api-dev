/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pina.app.model.AgendaStatus;
import com.pina.app.model.User;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Hp
 */
public class AgendaResponse {

    private Long id;
    private String title;
    private Date meeetDate;
    private String meeetInfo;
    private String location;
    private String address1;
    private String address2;
    private String additionalInfo;
    private AgendaStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getMeeetDate() {
        return meeetDate;
    }

    public void setMeeetDate(Date meeetDate) {
        this.meeetDate = meeetDate;
    }

    public String getMeeetInfo() {
        return meeetInfo;
    }

    public void setMeeetInfo(String meeetInfo) {
        this.meeetInfo = meeetInfo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public AgendaStatus getStatus() {
        return status;
    }

    public void setStatus(AgendaStatus status) {
        this.status = status;
    }

}
