package com.pina.app.payload.response;

import java.util.Date;

/**
 * Created by hp on 19/08/17.
 */
public class JwtAuthenticationResponse {

    private String accessToken;
    private String tokenType = "Bearer";
    private String lastLogin;
    private Integer fillState;
    private Integer starterState;

    public JwtAuthenticationResponse(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Integer getFillState() { return fillState; }

    public void setFillState(Integer fillState) { this.fillState = fillState; }

    public Integer getStarterState() { return starterState; }

    public void setStarterState(Integer starterState) { this.starterState = starterState; }

}
