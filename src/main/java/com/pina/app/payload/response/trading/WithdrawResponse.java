/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.response.trading;

import java.util.List;

/**
 *
 * @author Hp
 */
public class WithdrawResponse {

    private double cash;
    private double availableCash;
    private String bankName;
    private String bankAccountNo;
    private String bankAccountName;
    private String bankLogoUrl;
    private List<Withdraw> withdrawHistory;

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getAvailableCash() {
        return availableCash;
    }

    public void setAvailableCash(double availableCash) {
        this.availableCash = availableCash;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankLogoUrl() {
        return bankLogoUrl;
    }

    public void setBankLogoUrl(String bankLogoUrl) {
        this.bankLogoUrl = bankLogoUrl;
    }


    

    public List<Withdraw> getWithdrawHistory() {
        return withdrawHistory;
    }

    public void setWithdrawHistory(List<Withdraw> withdrawHistory) {
        this.withdrawHistory = withdrawHistory;
    }

}
