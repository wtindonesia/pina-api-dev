/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.response.newsfeed;

import java.util.List;

/**
 *
 * @author Hp
 */
public class NFPost {
    private long parentId;
    private String content;
    private String photoURLs;
    private String attachmentURLs;
    private List<NFComment> comments;
    private long sumLikes;
    private long sumComments;
    private long sumShares;



    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhotoURLs() {
        return photoURLs;
    }

    public void setPhotoURLs(String photoURLs) {
        this.photoURLs = photoURLs;
    }

    public String getAttachmentURLs() {
        return attachmentURLs;
    }

    public void setAttachmentURLs(String attachmentURLs) {
        this.attachmentURLs = attachmentURLs;
    }

    public List<NFComment> getComments() {
        return comments;
    }

    public void setComments(List<NFComment> comments) {
        this.comments = comments;
    }

    public long getSumLikes() {
        return sumLikes;
    }

    public void setSumLikes(long sumLikes) {
        this.sumLikes = sumLikes;
    }

    public long getSumComments() {
        return sumComments;
    }

    public void setSumComments(long sumComments) {
        this.sumComments = sumComments;
    }

    public long getSumShares() {
        return sumShares;
    }

    public void setSumShares(long sumShares) {
        this.sumShares = sumShares;
    }
    
    

}
