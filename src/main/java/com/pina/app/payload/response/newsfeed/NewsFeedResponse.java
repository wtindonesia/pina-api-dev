/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.response.newsfeed;

/**
 *
 * @author Hp
 */
public class NewsFeedResponse {

    private Long id;
    private Long parentId;
    private String content;
    private String photoURLs;
    private String attachmentURLs;
    private Integer sumLikes;
    private Integer sumComments;
    private Integer sumShares;
    private NewsFeedResponse parent;
    private NewsFeedUser user;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhotoURLs() {
        return photoURLs;
    }

    public void setPhotoURLs(String photoURLs) {
        this.photoURLs = photoURLs;
    }

    public String getAttachmentURLs() {
        return attachmentURLs;
    }

    public void setAttachmentURLs(String attachmentURLs) {
        this.attachmentURLs = attachmentURLs;
    }

    public Integer getSumLikes() {
        return sumLikes;
    }

    public void setSumLikes(Integer sumLikes) {
        this.sumLikes = sumLikes;
    }

    public Integer getSumComments() {
        return sumComments;
    }

    public void setSumComments(Integer sumComments) {
        this.sumComments = sumComments;
    }

    public Integer getSumShares() {
        return sumShares;
    }

    public void setSumShares(Integer sumShares) {
        this.sumShares = sumShares;
    }

    public NewsFeedUser getUser() {
        return user;
    }

    public void setUser(NewsFeedUser user) {
        this.user = user;
    }

    public NewsFeedResponse getParent() {
        return parent;
    }

    public void setParent(NewsFeedResponse parent) {
        this.parent = parent;
    }
    
    
    
    
}
