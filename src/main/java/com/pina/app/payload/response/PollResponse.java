package com.pina.app.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;

public class PollResponse {

    private Long id;
    private String question;
    private List<ChoiceResponse> choices;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long selectedChoice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<ChoiceResponse> getChoices() {
        return choices;
    }

    public void setChoices(List<ChoiceResponse> choices) {
        this.choices = choices;
    }

    public Long getSelectedChoice() {
        return selectedChoice;
    }

    public void setSelectedChoice(Long selectedChoice) {
        this.selectedChoice = selectedChoice;
    }

}
