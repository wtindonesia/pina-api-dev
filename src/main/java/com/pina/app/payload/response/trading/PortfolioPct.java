/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.payload.response.trading;

/**
 *
 * @author Hp
 */
public class PortfolioPct {
    private int stockPct;
    private int reksadanaPct;
    private int copyFundPct;

    public int getStockPct() {
        return stockPct;
    }

    public void setStockPct(int stockPct) {
        this.stockPct = stockPct;
    }

    public int getReksadanaPct() {
        return reksadanaPct;
    }

    public void setReksadanaPct(int reksadanaPct) {
        this.reksadanaPct = reksadanaPct;
    }

    public int getCopyFundPct() {
        return copyFundPct;
    }

    public void setCopyFundPct(int copyFundPct) {
        this.copyFundPct = copyFundPct;
    }
    
    
}
