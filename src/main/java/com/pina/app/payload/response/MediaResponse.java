package com.pina.app.payload.response;

/**
 * Get Media Response
 * @author      Sandi Andrian<andrian.sandi@gmail.com>
 * @since       Nov 8, 2019
 **/

public class MediaResponse {

    private String imageKtpUrl;
    private String imageKtpFaceUrl;
    private String imageNpwpUrl;
    private String imageSignatureUrl;

    public MediaResponse() { }

    public String getImageKtpUrl() { return imageKtpUrl; }
    public void setImageKtpUrl(String imageKtpUrl) { this.imageKtpUrl = imageKtpUrl; }

    public String getImageKtpFaceUrl() { return imageKtpFaceUrl; }
    public void setImageKtpFaceUrl(String imageKtpFaceUrl) { this.imageKtpFaceUrl = imageKtpFaceUrl; }

    public String getImageNpwpUrl() { return imageNpwpUrl; }
    public void setImageNpwpUrl(String imageNpwpUrl) { this.imageNpwpUrl = imageNpwpUrl; }

    public String getImageSignatureUrl() { return imageSignatureUrl; }
    public void setImageSignatureUrl(String imageSignatureUrl) { this.imageSignatureUrl = imageSignatureUrl; }
}
