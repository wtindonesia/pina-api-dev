package com.pina.app.payload;

public class UserSummary {
    private Long id;
    private String username;
    private String name;
    private Integer fillState;
    private Integer starterState;

    public UserSummary(Long id, String username, String name, Integer fillState, Integer starterState) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.fillState = fillState;
        this.starterState = starterState;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getfillState() { return fillState; }

    public void setFillState(Integer fillState) { this.fillState = fillState; }

    public Integer getStarterState() { return starterState; }

    public void setStarterState(Integer starterState) { this.starterState = starterState; }
}
