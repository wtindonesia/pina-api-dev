/**
 *
 * @author  Sandi Andrian <andrian.sandi@gmail.com>
 * @since   Nov 19, 2019
 **/
package com.pina.app.model;

import com.pina.app.model.audit.DateAudit;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@TypeDefs({
    @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})

@Entity
@Table(name = "survey_answers")
public class SurveyAnswer extends DateAudit {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private List<SurveyAnswerChoice> answers;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public UUID getId() { return id; }

    public void setId(UUID id) { this.id = id; }

    public List<SurveyAnswerChoice> getAnswers() {
        return answers;
    }

    public void setAnswers(List<SurveyAnswerChoice> answers) {
        this.answers = answers;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
