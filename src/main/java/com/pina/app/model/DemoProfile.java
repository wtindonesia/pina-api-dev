package com.pina.app.model;

import com.pina.app.model.UserProfile;
import com.pina.app.model.audit.DateAudit;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author AS
 */

@TypeDefs({ @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class) })
@Entity
@Table(name = "demo")        
public class DemoProfile extends DateAudit{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 100)
    @Column(name = "key")
    private String key;

    @Type(type = "jsonb")
    @Column(name = "value", columnDefinition = "jsonb")
    private Map<String, Object> value;

    //@NotBlank
    @Column(name = "user_id")
    private Long userId;

    // public DemoProfile(String key, String value, Long userId) {
    //     this.key = key;
    //     this.value = value;
    //     this.userId = userId;
    // }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Map<String, Object> getValue() {
        return value;
    }

    public void setValue(Map<String, Object> value) {
        this.value = value;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

}