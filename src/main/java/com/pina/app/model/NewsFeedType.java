/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.model;

import java.util.Arrays;

/**
 *
 * @author Hp
 */
public enum NewsFeedType {
    POST("post"),
    SHARE("share"),
    COMMENT("comment"),
    LIKE("like");

    private String value;

    private NewsFeedType(String value) {
        this.value = value;
    }

    public static NewsFeedType fromValue(String value) {
        for (NewsFeedType type : values()) {
            if (type.value.equalsIgnoreCase(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException(
                "Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
