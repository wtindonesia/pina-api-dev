/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.model;

import java.util.Arrays;

/**
 *
 * @author Hp
 */
public enum QType {
    OPTION_MULTI("option_multi"),
    OPTION_SINGLE("option_single"),
    INPUT("input");

    private String value;

    private QType(String value) {
        this.value = value;
    }

    public static QType fromValue(String value) {
        for (QType type : values()) {
            if (type.value.equalsIgnoreCase(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException(
                "Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    
}
