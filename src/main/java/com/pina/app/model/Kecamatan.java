/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pina.app.model.audit.DateAudit;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 *
 * @author Hp
 */
@Entity
@Table(name = "kecamatans")
public class Kecamatan extends DateAudit {

    public Kecamatan() {}

    public Kecamatan(City city,String name) {
        this.name = name;
        this.city = city;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(max = 255)
    private String name;

    @ManyToOne
    @JoinColumn(name = "city_id")
    @JsonIgnore
    private City city;

    //kecamatan itu model di java -nya
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kecamatan", orphanRemoval = true)
    private List<Kelurahan> kelurahans = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<Kelurahan> getKelurahans() {
        return kelurahans;
    }

    public void setKelurahans(List<Kelurahan> kelurahans) {
        this.kelurahans = kelurahans;
    }

}
