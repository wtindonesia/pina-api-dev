package com.pina.app.model;

/**
 * Created by hp on 07/12/17.
 */
public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
