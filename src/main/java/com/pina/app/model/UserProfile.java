/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.model;

import com.pina.app.model.audit.DateAudit;
import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeStringType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

/**
 *
 * @author Hp
 */
@TypeDefs({
    @TypeDef(name = "string-array", typeClass = StringArrayType.class)
    ,
    @TypeDef(name = "int-array", typeClass = IntArrayType.class)
    ,
    @TypeDef(name = "json", typeClass = JsonStringType.class)
    ,
    @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
    ,
    @TypeDef(name = "jsonb-node", typeClass = JsonNodeBinaryType.class)
    ,
    @TypeDef(name = "json-node", typeClass = JsonNodeStringType.class),})

@Entity
@Table(name = "user_profiles")
public class UserProfile extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 255)
    @Column(name = "full_name")
    private String fullName;

    @Size(max = 15)
    @Column(name = "phone_no")
    private String phoneNo;

    @Size(max = 30)
    @Column(name = "ktp_no")
    private String ktpNo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ktp_expired")
    private Date ktpExpired;

    @Column(name = "ktp_lifetime")
    private Boolean ktpLifetime;

    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date")
    private Date birthDate;

    @Size(max = 255)
    @Column(name = "birth_place")
    private String birthPlace;

    @Size(max = 200)
    @Column(name = "nationality")
    private String nationality;

    @Size(max = 255)
    private String address;

    @Column(name = "province_id")
    private Integer provinceId;

    @Column(name = "city_id")
    private Integer cityId;

    @Column(name = "kec_id")
    private Long kecId;

    @Column(name = "kel_id")
    private Long kelId;

    private String rtrw;

    @Size(max = 7)
    @Column(name = "zip_code")
    private String zipCode;

    @Size(max = 200)
    @Column(name = "img_ktp_url")
    private String imgKtpUrl;

    @Size(max = 200)
    @Column(name = "img_ktpface_url")
    private String imgFaceUrl;

    @Size(max = 200)
    @Column(name = "img_signature_url")
    private String imgSignatureUrl;

    @Size(max = 200)
    @Column(name = "img_npwp_url")
    private String imgNpwpUrl;

    @Size(max = 200)
    @Column(name = "img_avatar_url")
    private String imgAvatarUrl;

    @Column(name = "rdnbank_id")
    private Integer rdnbankId;

    @Size(max = 200)
    @Column(name = "rdnbank_account_no")
    private String rdnbankAccountNo;

    @Size(max = 200)
    @Column(name = "rdnbank_account_holder")
    private String rdnbankAccountHolder;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private UserSurvey survey;

    @Column(name = "fill_state")
    private Integer fillState;

    @Column(name = "starter_state", nullable = true, columnDefinition = "int default 0")
    private Integer starterState;

    @Column(name = "purpose_of_account")
    private String purposeOfAccount;

    @Column(name = "source_funds")
    private String sourceFunds;

    @Column(name = "monthly_income")
    private String monthlyIncome;

    @Column(name = "monthly_spending")
    private String monthlySpending;

    @Column(name = "occupation")
    private String occupation;

    @Column(name = "industry")
    private String industry;

    @Column(name = "position")
    private String position;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "company_address")
    private String companyAddress;

    @Column(name = "company_province", nullable = true, columnDefinition = "int default 0")
    private int companyProvince;

    @Column(name = "company_city", nullable = true, columnDefinition = "int default 0")
    private int companyCity;

    @Column(name = "company_phone")
    private String companyPhone;

    public Long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getKtpNo() {
        return ktpNo;
    }

    public void setKtpNo(String ktpNo) {
        this.ktpNo = ktpNo;
    }

    public Date getKtpExpired() {
        return ktpExpired;
    }

    public void setKtpExpired(Date ktpExpired) {
        this.ktpExpired = ktpExpired;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Long getKecId() {
        return kecId;
    }

    public void setKecId(Long kecId) {
        this.kecId = kecId;
    }

    public Long getKelId() {
        return kelId;
    }

    public void setKelId(Long kelId) {
        this.kelId = kelId;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getImgKtpUrl() {
        return imgKtpUrl;
    }

    public void setImgKtpUrl(String imgKtpUrl) {
        this.imgKtpUrl = imgKtpUrl;
    }

    public String getImgFaceUrl() {
        return imgFaceUrl;
    }

    public void setImgFaceUrl(String imgFaceUrl) {
        this.imgFaceUrl = imgFaceUrl;
    }

    public String getImgSignatureUrl() {
        return imgSignatureUrl;
    }

    public void setImgSignatureUrl(String imgSignatureUrl) {
        this.imgSignatureUrl = imgSignatureUrl;
    }

    public String getImgNpwpUrl() {
        return imgNpwpUrl;
    }

    public void setImgNpwpUrl(String imgNpwpUrl) {
        this.imgNpwpUrl = imgNpwpUrl;
    }

    public Integer getRdnbankId() {
        return rdnbankId;
    }

    public void setRdnbankId(Integer rdnbankId) {
        this.rdnbankId = rdnbankId;
    }

    public String getRdnbankAccountNo() {
        return rdnbankAccountNo;
    }

    public void setRdnbankAccountNo(String rdnbankAccountNo) {
        this.rdnbankAccountNo = rdnbankAccountNo;
    }

    public String getRdnbankAccountHolder() {
        return rdnbankAccountHolder;
    }

    public void setRdnbankAccountHolder(String rdnbankAccountHolder) {
        this.rdnbankAccountHolder = rdnbankAccountHolder;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getImgAvatarUrl() {
        return imgAvatarUrl;
    }

    public void setImgAvatarUrl(String imgAvatarUrl) {
        this.imgAvatarUrl = imgAvatarUrl;
    }

    public Boolean getKtpLifetime() {
        return ktpLifetime;
    }

    public void setKtpLifetime(Boolean ktpLifetime) {
        this.ktpLifetime = ktpLifetime;
    }

    public UserSurvey getSurvey() {
        return survey;
    }

    public void setSurvey(UserSurvey survey) {
        this.survey = survey;
    }

    public Integer getFillState() {
        return fillState;
    }

    public void setFillState(Integer fillState) {
        this.fillState = fillState;
    }

    public String getRtrw() {
        return rtrw;
    }

    public void setRtrw(String rtrw) {
        this.rtrw = rtrw;
    }

    public Integer getStarterState() { return starterState; }

    public void setStarterState(Integer starterState) { this.starterState = starterState; }

    public String getPurposeOfAccount() { return purposeOfAccount; }
    public void setPurposeOfAccount(String purposeOfAccount) { this.purposeOfAccount = purposeOfAccount; }

    public String getSourceFunds() { return sourceFunds; }
    public void setSourceFunds(String sourceFunds) { this.sourceFunds = sourceFunds; }

    public String getMonthlyIncome() { return monthlyIncome; }
    public void setMonthlyIncome(String monthlyIncome) { this.monthlyIncome = monthlyIncome; }

    public String getMonthlySpending() { return monthlySpending; }
    public void setMonthlySpending(String monthlySpending) { this.monthlySpending = monthlySpending; }

    public String getOccupation() { return occupation; }
    public void setOccupation(String occupation) { this.occupation = occupation; }

    public String getIndustry() { return industry; }
    public void setIndustry(String industry) { this.industry = industry; }

    public String getPosition() { return position; }
    public void setPosition(String position) { this.position = position; }

    public String getCompanyName() { return companyName; }
    public void setCompanyName(String companyName) { this.companyName = companyName; }

    public String getCompanyAddress() { return companyAddress; }
    public void setCompanyAddress(String companyAddress) { this.companyAddress = companyAddress; }

    public int getCompanyProvince() { return companyProvince; }
    public void setCompanyProvince(int companyProvince) { this.companyProvince = companyProvince; }

    public int getCompanyCity() { return companyCity; }
    public void setCompanyCity(int companyCity) { this.companyCity = companyCity; }

    public String getCompanyPhone() { return companyPhone; }
    public void setCompanyPhone(String companyPhone) { this.companyPhone = companyPhone; }
}
