package com.pina.app.model;

import com.pina.app.model.audit.DateAudit;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.util.ArrayList;
import java.util.Date;
import org.hibernate.annotations.NaturalId;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

/**
 * Created by hp.
 */
@TypeDefs({
    @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)})

@Entity
@Table(name = "users", uniqueConstraints = {
    @UniqueConstraint(columnNames = {
        "username"
    })
    ,
        @UniqueConstraint(columnNames = {
        "email"
    })
})
public class User extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 255)
    @Column(name = "username")
    private String userName;

    @NaturalId
    @NotBlank
    @Size(max = 255)
    @Email
    private String email;

    @NotBlank
    @Size(max = 100)
    private String password;

    @Size(max = 100)
    private String pin;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "follow_users",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "follow_user_id", referencedColumnName = "id"))
    private Set<User> followUsers = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "follow_stocks",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "follow_stock_id", referencedColumnName = "id"))
    private Set<Stock> followStocks = new HashSet<>();

    @Column(name = "reset_password_token")
    private String resetPasswordToken;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reset_password_sent_at")
    private Date resetPasswordSentAt;

    @Column(name = "verification_code")
    private String verificationCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_login")
    private Date lastLogin;

    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "user")
    private UserProfile userProfile;

    //-------------------------------------------------------------------------
    // NEWSFEED RELATION
    //-------------------------------------------------------------------------
    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Comment> comments = new ArrayList<>();

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<NewsFeed> newsFeeds = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            },
            mappedBy = "userShares")
    private Set<NewsFeed> shares = new HashSet<>();

    //mappedBy di isi dengan nama object lawannya -- bidirectional
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            },
            mappedBy = "userLikes")
    private Set<NewsFeed> likes = new HashSet<>();

    //-------------------------------------------------------------------------
    // NEWSFEED RELATION
    //-------------------------------------------------------------------------
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Preference preference;

    @Type(type = "jsonb")
    @Column(name = "facebook_signup", columnDefinition = "jsonb")
    private FacebookSignup facebookSignup;

    @Column(name = "real_account", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean realAccount = false;

    @Column(name = "real_account_active", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean realAccountActive = false;

    @Column(name = "pin_created", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean pinCreated = false;

    @Column(name = "apply_as_influencer", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean applyAsInfluencer = false;

    @Column(name = "influencer", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean influencer = false;

    @Column(name = "is_verified_phone", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean isVerifiedPhone = false;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", orphanRemoval = true)
    private List<Agenda> agendas = new ArrayList<>();

    public User() {

    }

    public User(String username, String email, String password) {
        this.userName = username;
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String username) {
        this.userName = username;
    }

    public String getName() {
        if (getUserProfile() != null) {
            if (getUserProfile().getFullName() != null) {
                if (!getUserProfile().getFullName().equals("")) {
                    return getUserProfile().getFullName();
                }
            }
        }
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<User> getFollowUsers() {
        return followUsers;
    }

    public void setFollowUsers(Set<User> followUsers) {
        this.followUsers = followUsers;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public Date getResetPasswordSentAt() {
        return resetPasswordSentAt;
    }

    public void setResetPasswordSentAt(Date resetPasswordSentAt) {
        this.resetPasswordSentAt = resetPasswordSentAt;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public Set<Stock> getFollowStocks() {
        return followStocks;
    }

    public void setFollowStocks(Set<Stock> followStocks) {
        this.followStocks = followStocks;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public Preference getPreference() {
        return preference;
    }

    public void setPreference(Preference preference) {
        this.preference = preference;
    }

    public FacebookSignup getFacebookSignup() {
        return facebookSignup;
    }

    public void setFacebookSignup(FacebookSignup facebookSignup) {
        this.facebookSignup = facebookSignup;
    }

    public boolean getRealAccount() {
        return realAccount;
    }

    public void setRealAccount(Boolean realAccount) {
        this.realAccount = realAccount;
    }

    public List<Agenda> getAgendas() {
        return agendas;
    }

    public void setAgendas(List<Agenda> agendas) {
        this.agendas = agendas;
    }

    public Boolean getRealAccountActive() {
        return realAccountActive;
    }

    public void setRealAccountActive(Boolean realAccountActive) {
        this.realAccountActive = realAccountActive;
    }

    public Boolean getPinCreated() {
        return pinCreated;
    }

    public void setPinCreated(Boolean pinCreated) {
        this.pinCreated = pinCreated;
    }

    public Boolean getApplyAsInfluencer() {
        return applyAsInfluencer;
    }

    public void setApplyAsInfluencer(Boolean applyAsInfluencer) {
        this.applyAsInfluencer = applyAsInfluencer;
    }

    public Boolean getInfluencer() {
        return influencer;
    }

    public void setInfluencer(Boolean influencer) {
        this.influencer = influencer;
    }

    public List<NewsFeed> getNewsFeeds() {
        return newsFeeds;
    }

    public void setNewsFeeds(List<NewsFeed> newsFeeds) {
        this.newsFeeds = newsFeeds;
    }

    public Set<NewsFeed> getShares() {
        return shares;
    }

    public void setShares(Set<NewsFeed> shares) {
        this.shares = shares;
    }

    public Set<NewsFeed> getLikes() {
        return likes;
    }

    public void setLikes(Set<NewsFeed> likes) {
        this.likes = likes;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Boolean getIsVerifiedPhone() { return isVerifiedPhone; }

    public void setIsVerifiedPhone(Boolean isVerifiedPhone) { this.isVerifiedPhone = isVerifiedPhone; }

}
