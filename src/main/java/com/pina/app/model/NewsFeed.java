/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.model;

import com.pina.app.model.audit.DateAudit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Hp
 */
@Entity
@Table(name = "news_feeds")
public class NewsFeed extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "parent_id")
    private Long parentId;

    @Column(columnDefinition = "TEXT")
    private String content;

    //post/share
    @Enumerated(EnumType.STRING)
    private NewsFeedType type = NewsFeedType.POST;

    //private/public
    @Enumerated(EnumType.STRING)
    private NewsFeedAccess access = NewsFeedAccess.PUBLIC;

    @Column(name = "photo_urls")
    private String photoURLs;
    @Column(name = "attachment_urls")
    private String attachmentURLs;

    @Column(name = "sum_likes", columnDefinition = "INTEGER default '0'")
    private Integer sumLikes = 0;

    @Column(name = "sum_comments", columnDefinition = "INTEGER default '0'")
    private Integer sumComments = 0;

    @Column(name = "sum_shares", columnDefinition = "INTEGER default '0'")
    private Integer sumShares = 0;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "news_feed_comments",
            joinColumns = @JoinColumn(name = "news_feed_id"),
            inverseJoinColumns = @JoinColumn(name = "comment_id"))
    private List<Comment> comments = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            })
    @JoinTable(name = "news_feed_shares",
            joinColumns = @JoinColumn(name = "news_feed_id"),
            inverseJoinColumns = @JoinColumn(name = "share_by_user_id"))
    private Set<User> userShares = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            })
    @JoinTable(name = "news_feed_likes",
            joinColumns = @JoinColumn(name = "news_feed_id"),
            inverseJoinColumns = @JoinColumn(name = "like_by_user_id"))
    private Set<User> userLikes = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId != null ? parentId : 0;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSumLikes() {
        return sumLikes;
    }

    public void setSumLikes(Integer sumLikes) {
        this.sumLikes = sumLikes;
    }

    public Integer getSumComments() {
        return sumComments;
    }

    public void setSumComments(Integer sumComments) {
        this.sumComments = sumComments;
    }

    public Integer getSumShares() {
        return sumShares;
    }

    public void setSumShares(Integer sumShares) {
        this.sumShares = sumShares;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getPhotoURLs() {
        return photoURLs;
    }

    public void setPhotoURLs(String photoURLs) {
        this.photoURLs = photoURLs;
    }

    public String getAttachmentURLs() {
        return attachmentURLs;
    }

    public void setAttachmentURLs(String attachmentURLs) {
        this.attachmentURLs = attachmentURLs;
    }

    public Set<User> getUserShares() {
        return userShares;
    }

    public void setUserShares(Set<User> userShares) {
        this.userShares = userShares;
    }

    public Set<User> getUserLikes() {
        return userLikes;
    }

    public void setUserLikes(Set<User> userLikes) {
        this.userLikes = userLikes;
    }

    public NewsFeedType getType() {
        return type;
    }

    public void setType(NewsFeedType type) {
        this.type = type;
    }

    public NewsFeedAccess getAccess() {
        return access;
    }

    public void setAccess(NewsFeedAccess access) {
        this.access = access;
    }

}
