package com.pina.app.model;

import java.io.Serializable;

/**
 *
 * @author  Sandi Andrian <andrian.sandi@gmail.com>
 * @since   Nov 19, 2019
 **/

public class SurveyAnswerChoice implements Serializable {
    private int question;
    private int choice;

    public int getQuestion() { return question; }
    public void setQuestion(int question) { this.question = question; }

    public int getChoice() { return choice; }
    public void setChoice(int choice) { this.choice = choice; }
}
