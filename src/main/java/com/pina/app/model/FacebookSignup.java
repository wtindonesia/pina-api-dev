/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.model;

import java.io.Serializable;

/**
 *
 * @author Hp
 */
public class FacebookSignup implements Serializable {

    private String id;
    private String email;
    private String name;
    private String photo;

    public FacebookSignup(String id, String email, String name, String photo) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.photo = photo;
    }

    public FacebookSignup() {
    }

    
    
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
