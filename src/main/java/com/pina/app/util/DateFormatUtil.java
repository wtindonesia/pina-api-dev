/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.util;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author Hp
 */
public class DateFormatUtil {

    public static String format(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(date);

    }

    public static String format(Instant instantDate) {
        Date date = Date.from(instantDate);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(date);

    }

    public static String formatAgenda(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMMM YYYY . hh:mm a");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(date);

    }
}
