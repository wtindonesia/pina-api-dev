package com.pina.app.util;

import com.pina.app.model.Agenda;
import com.pina.app.model.City;
import com.pina.app.model.Kecamatan;
import com.pina.app.model.Kelurahan;
import com.pina.app.model.NewsFeed;
import com.pina.app.model.Poll;
import com.pina.app.model.Stock;
import com.pina.app.model.User;
import com.pina.app.payload.response.AgendaResponse;
import com.pina.app.payload.response.ChoiceResponse;
import com.pina.app.payload.response.CityResponse;
import com.pina.app.payload.response.FollowStockResponse;
import com.pina.app.payload.response.PollResponse;
import com.pina.app.payload.response.FollowUserResponse;
import com.pina.app.payload.response.KecamatanResponse;
import com.pina.app.payload.response.KelurahanResponse;
import com.pina.app.payload.response.LocationResponse;
import com.pina.app.payload.response.newsfeed.NewsFeedResponse;
import com.pina.app.payload.response.newsfeed.NewsFeedUser;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class ModelMapper {

    public static FollowUserResponse getFollowUser(User user, boolean follow) {
        FollowUserResponse followUser = new FollowUserResponse();
        followUser.setFollow(follow);
        followUser.setUserId(user.getId());
        followUser.setName(user.getName());
        followUser.setPercent("20%");
        followUser.setPhoto("https://pbs.twimg.com/profile_images/491390735749378050/ZK5nwUPR_400x400.jpeg");
        followUser.setReturns("Return (Last 1294)");
        return followUser;
    }

    public static FollowUserResponse mapFollowUserResponse(Set<User> listFollowed, User user) {
        return getFollowUser(user, isFollowUser(listFollowed, user));
    }

    public static boolean isFollowUser(Set<User> listFollowUser, User user) {
        if (listFollowUser == null) {
            return false;
        }
        return listFollowUser.stream().anyMatch((u) -> (Objects.equals(u.getId(), user.getId())));
    }

    public static List<FollowUserResponse> mapFollowUserResponse(Set<User> listFollowed) {
        List<FollowUserResponse> result = listFollowed.stream().map(user -> {
            return getFollowUser(user, true);
        }).collect(Collectors.toList());

        return result;
    }

    public static FollowStockResponse getFollowStock(Stock stock, boolean follow) {
        FollowStockResponse followStock = new FollowStockResponse();
        followStock.setId(stock.getId());
        followStock.setCode(stock.getCode());
        followStock.setName(stock.getName());
        followStock.setFollow(follow);
        followStock.setLogoUrl(stock.getLogoUrl());
        return followStock;

    }

    public static boolean isFollowStock(Set<Stock> listFollowStock, Stock stock) {
        if (listFollowStock == null) {
            return false;
        }
        return listFollowStock.stream().anyMatch((s) -> (Objects.equals(s.getId(), stock.getId())));
    }

    public static FollowStockResponse mapFollowStockResponse(Set<Stock> listFollowed, Stock stock) {
        return getFollowStock(stock, isFollowStock(listFollowed, stock));
    }

    public static List<FollowStockResponse> mapFollowStockResponse(Set<Stock> listFollowed) {
        List<FollowStockResponse> result = listFollowed.stream().map(stock -> {
            return getFollowStock(stock, true);
        }).collect(Collectors.toList());

        return result;
    }

    public static CityResponse mapCityResponse(City city) {
        CityResponse res = new CityResponse();
        res.setId(city.getId());
        res.setName(city.getName());
        res.setKecamatans(city.getKecamatans());
        Map<String, Object> mapProvince = new HashMap<>();
        mapProvince.put("province", new LocationResponse(city.getProvince()
                .getId(), city.getProvince().getName()));
        res.setParent(mapProvince);
        return res;
    }

    public static KecamatanResponse mapKecamatanResponse(Kecamatan kec) {
        KecamatanResponse res = new KecamatanResponse();
        res.setId(kec.getId());
        res.setName(kec.getName());
        res.setKelurahans(kec.getKelurahans());
        Map<String, Object> mapProvince = new HashMap<>();
        mapProvince.put("city", new LocationResponse(kec.getCity().getId(), kec.getCity().getName()));
        mapProvince.put("province", new LocationResponse(kec.getCity().getProvince()
                .getId(), kec.getCity().getProvince().getName()));
        res.setParent(mapProvince);
        return res;
    }

    public static KelurahanResponse mapKelurahanResponse(Kelurahan kel) {
        KelurahanResponse res = new KelurahanResponse();
        res.setId(kel.getId());
        res.setName(kel.getName());
        Map<String, Object> mapProvince = new HashMap<>();
        mapProvince.put("kecamatan", new LocationResponse(kel.getKecamatan().getId(), kel.getKecamatan().getName()));
        mapProvince.put("city", new LocationResponse(kel.getKecamatan().getCity().getId(), kel.getKecamatan().getCity().getName()));
        mapProvince.put("city", new LocationResponse(kel.getKecamatan().getCity().getId(), kel.getKecamatan().getCity().getName()));
        mapProvince.put("province", new LocationResponse(kel.getKecamatan().getCity().getProvince()
                .getId(), kel.getKecamatan().getCity().getProvince().getName()));
        res.setParent(mapProvince);
        return res;
    }

    public static PollResponse mapPollToPollResponse(Poll poll, Long userVote) {
        PollResponse pollResponse = new PollResponse();
        pollResponse.setId(poll.getId());
        pollResponse.setQuestion(poll.getQuestion());
        List<ChoiceResponse> choiceResponses = poll.getChoices().stream().map(choice -> {
            ChoiceResponse choiceResponse = new ChoiceResponse();
            choiceResponse.setId(choice.getId());
            choiceResponse.setText(choice.getText());
            return choiceResponse;
        }).collect(Collectors.toList());
        pollResponse.setChoices(choiceResponses);
        if (userVote != null) {
            pollResponse.setSelectedChoice(userVote);
        }
        return pollResponse;
    }

    public static List<AgendaResponse> mapAgendaResponse(List<Agenda> listAgenda) {
        List<AgendaResponse> res = new ArrayList<>();
        listAgenda.forEach((a) -> {
            res.add(mapAgendaResponse(a));
        });
        return res;
    }

    public static AgendaResponse mapAgendaResponse(Agenda agenda) {

        String agendaInfo = DateFormatUtil.formatAgenda(agenda.getMeeetDate());
        agendaInfo += "\n At " + agenda.getAddress1();
        agendaInfo = agendaInfo.replace("AM", "a.m").replace("PM", "p.m");

        AgendaResponse res = new AgendaResponse();
        res.setId(agenda.getId());
        res.setAdditionalInfo(agenda.getAdditionalInfo());
        res.setAddress1(agenda.getAddress1());
        res.setAddress2(agenda.getAddress2());
        res.setLocation(agenda.getLocation());
        res.setMeeetDate(agenda.getMeeetDate());
        res.setMeeetInfo(agendaInfo);
        res.setStatus(agenda.getStatus());
        res.setTitle(agenda.getTitle());
        return res;
    }

    public static NewsFeedResponse mapNewsFeedResponse(NewsFeed newsfeed) {
        NewsFeedResponse r = new NewsFeedResponse();
        r.setId(newsfeed.getId());
        r.setContent(newsfeed.getContent());
        r.setParentId(newsfeed.getParentId());
        r.setPhotoURLs(newsfeed.getPhotoURLs());
        r.setAttachmentURLs(newsfeed.getAttachmentURLs());
        r.setSumComments(newsfeed.getSumComments());
        r.setSumLikes(newsfeed.getSumLikes());
        r.setSumShares(newsfeed.getSumShares());
        r.setUser(new NewsFeedUser());
        r.getUser().setId(newsfeed.getUser().getId());
        r.getUser().setInfluencer(newsfeed.getUser().getInfluencer());
        r.getUser().setImgAvatarUrl(newsfeed.getUser().getUserProfile().getImgAvatarUrl());
        return r;
    }

}
