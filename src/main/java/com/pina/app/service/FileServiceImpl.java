/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.model.User;
import com.pina.app.model.UserProfile;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.UploadFileResponse;
import com.pina.app.repository.UserProfileRep;
import com.pina.app.repository.UserRep;
import com.pina.app.security.CurrentUser;
import com.pina.app.security.UserPrincipal;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Hp
 */
@Service("FileService")
public class FileServiceImpl implements FileService {

    private static final Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

    @Autowired
    UserRep userRepository;

    @Autowired
    private UserProfileRep userProfileRepo;

    @Autowired
    private FileStorageService fileStorageService;

    @Override
    public ResponseEntity<?> upload(MultipartFile file, String folder, UserPrincipal currentUser) {
        String fileName = fileStorageService.storeFile(file, folder);
        String fileUrl = "BASE_URL/" + folder + "/" + fileName;
        UploadFileResponse result = new UploadFileResponse(fileName, fileUrl, file.getContentType(), file.getSize());

        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        User user = userOpt.get();
        UserProfile userProfile = user.getUserProfile();

        switch (folder) {
            case "ktp":
                userProfile.setImgKtpUrl(fileUrl);
                break;
            case "selfie":
                userProfile.setImgFaceUrl(fileUrl);
                break;
            case "npwp":
                userProfile.setImgNpwpUrl(fileUrl);
                break;
            case "signature":
                userProfile.setImgSignatureUrl(fileUrl);
                break;
        }

        userRepository.save(user);

        return ResponseEntity.ok(new ApiResponse(true, null, "", result));
    }

    @Override
    public ResponseEntity<?> getFile(String fileName, String folder, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName, folder);
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }
        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @Override
    public ResponseEntity<byte[]> getImage(String fileName, String folder) {
        Resource resource = fileStorageService.loadFileAsResource(fileName, folder);
        byte[] byteArray = null;
        try {
            byteArray = Files.readAllBytes(resource.getFile().toPath());
        } catch (IOException ex) {
            logger.error(ex.toString());
        }
        return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(byteArray);
    }

    @Override
    public ResponseEntity<?> uploadAvatar(UserPrincipal currentUser, MultipartFile file, String folder) {
        String fileName = fileStorageService.storeFile(file, folder);
        String fileUrl = "BASE_URL/images/" + folder + "/" + fileName;

        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }
        User user = userOpt.get();
        UserProfile userProfile = user.getUserProfile();
        if (userProfile == null) {
            userProfile = new UserProfile();
            userProfile.setUser(user);
        }

        userProfile.setImgAvatarUrl(fileUrl);
        userProfileRepo.save(userProfile);

        UploadFileResponse result = new UploadFileResponse(fileName, fileUrl, file.getContentType(), file.getSize());
        return ResponseEntity.ok(new ApiResponse(true, null, "", result));
    }

}
