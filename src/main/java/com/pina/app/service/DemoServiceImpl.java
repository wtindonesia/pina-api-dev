package com.pina.app.service;

import java.util.List;
import java.util.stream.Collectors;

import com.pina.app.model.DemoProfile;
import com.pina.app.payload.request.DemoRequest;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.DemoResponse;
import com.pina.app.repository.DemoRepo;
import com.pina.app.security.UserPrincipal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("DemoService")
public class DemoServiceImpl implements DemoService {


    @Autowired
    DemoRepo demoRepo;

    public ResponseEntity<?> getDemoById(UserPrincipal currentUser){
        List<DemoProfile> demoProfile = demoRepo.findByUserId(currentUser.getId());

        List<DemoResponse> resList = demoProfile.stream().map(demo -> {
            DemoResponse response = new DemoResponse();
            response.setKey(demo.getKey());
            response.setValue(demo.getValue());
            response.setUserId(demo.getUserId());
            return response;
        }).collect(Collectors.toList());

        return ResponseEntity.ok(new ApiResponse(true, null, "", resList));
    }

    @Override
    public ResponseEntity<?> postDemo(DemoRequest body, UserPrincipal currentUser) {

        DemoProfile demoProfile = new DemoProfile();
        demoProfile.setKey(body.getKey());
        demoProfile.setValue(body.getValue());
        demoProfile.setUserId(currentUser.getId());
        demoRepo.save(demoProfile);

        // URI location =
        // ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/demo/{key}")
        // .buildAndExpand(body.getKey()).toUri();
        // logger.info("locasiya : "+ location);

        return ResponseEntity.created(null)
                .body(new ApiResponse(true, null, "Post successfully.", null));
    }

    public ResponseEntity<?> getAllByKey(String key) {
        List<DemoProfile> demoProfile = demoRepo.findByKey(key);

        List<DemoResponse> resList = demoProfile.stream().map(demo -> {
            DemoResponse response = new DemoResponse();
            response.setKey(demo.getKey());
            response.setValue(demo.getValue());
            response.setUserId(demo.getUserId());
            return response;
        }).collect(Collectors.toList());

        return ResponseEntity.ok(new ApiResponse(true, null, "", resList));
    }
}