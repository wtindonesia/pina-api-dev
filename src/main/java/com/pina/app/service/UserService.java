/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.model.User;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Hp
 */


public interface UserService {

    public Optional<User> findUserByEmail(String email);

    public Optional<User> findUserByVerificationCode(String code);

    public Optional<User> findUserByVerificationCodeAndEmail(String code, String email);

    public void save(User user);

    public void followUser(Long userId, List<Long> listUserId);

    public void unfollowUser(Long userId, List<Long> listUserId);
    
    public void followStock(Long userId, List<Long> listStockId);

    public void unfollowStock(Long userId, List<Long> listStockId);
}
