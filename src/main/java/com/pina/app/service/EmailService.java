/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;


import org.springframework.mail.SimpleMailMessage;

/**
 *
 * @author Hp
 */

public interface EmailService {
    public void sendEmail(SimpleMailMessage email);
}
