/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.payload.request.LoginPinRequest;
import com.pina.app.payload.request.PinAddRequest;
import com.pina.app.payload.request.PinCheckRequest;
import com.pina.app.payload.request.WithdrawRequest;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author Hp
 */
public interface RealTradingService {

    ResponseEntity<?> loginPin(LoginPinRequest body, long userId);

    ResponseEntity<?> logoutPin(long userId);
    
    ResponseEntity<?> checkSession(PinCheckRequest body);

    ResponseEntity<?> createPin(PinAddRequest body, long userId);

    ResponseEntity<?> getPortfolio(long userId);

    ResponseEntity<?> getDeposit(long userId);

    ResponseEntity<?> getWithdraw(long userId);

    ResponseEntity<?> reqWithdraw(WithdrawRequest body, long userId);
    ResponseEntity<?> checkToken(String token); 

}
