/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.security.UserPrincipal;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Hp
 */
public interface FileService {

    ResponseEntity<?> uploadAvatar(UserPrincipal currentUser, MultipartFile file, String folder);

    ResponseEntity<?> upload(MultipartFile file, String folder,UserPrincipal currentUser);

    ResponseEntity<byte[]> getImage(String fileName, String folder);
    ResponseEntity<?> getFile(String fileName, String folder, HttpServletRequest request);
}
