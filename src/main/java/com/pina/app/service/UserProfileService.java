/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.model.UserProfile;

/**
 *
 * @author Hp
 */
public interface UserProfileService {
    public void save(UserProfile userProfile);
    public void update(UserProfile userProfile);
}
