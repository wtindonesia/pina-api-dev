/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.model.FAQ;
import com.pina.app.payload.request.FAQRequest;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.repository.FAQRep;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hp
 */
@Service("FAQService")
public class FAQServiceImpl implements FAQService {

    @Autowired
    private FAQRep faqRep;

    @Override
    public ResponseEntity<?> getFAQ(String name) {
        List<FAQ> listFAQ = faqRep.findAllByName(name);
        return ResponseEntity.ok(new ApiResponse(true, null, "", listFAQ));
    }

    @Override
    public ResponseEntity<?> addFAQ(FAQRequest body) {
        FAQ faq = new FAQ();
        faq.setName(body.getName());
        faq.setContent(body.getContent());
        faqRep.save(faq);
        return ResponseEntity.created(null).body(new ApiResponse(true, null, "FAQ Created Successfully", null));
    }

}
