/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.model.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pina.app.repository.UserProfileRep;

/**
 *
 * @author Hp
 */
@Service("UserProfileService")
public class UserProfileServiceImpl implements UserProfileService{

    @Autowired
    private UserProfileRep userProfileRepo;
    
    @Override
    public void save(UserProfile userProfile) {
        userProfileRepo.save(userProfile);
    }

    @Override
    public void update(UserProfile userProfile) {
        userProfileRepo.save(userProfile);
    }
    
}
