/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.model.Stock;
import com.pina.app.model.User;
import com.pina.app.payload.request.LoginPinRequest;
import com.pina.app.payload.request.PinAddRequest;
import com.pina.app.payload.request.PinCheckRequest;
import com.pina.app.payload.request.WithdrawRequest;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.CheckTokenResponse;
import com.pina.app.payload.response.LoginPinResponse;
import com.pina.app.payload.response.trading.Deposit;
import com.pina.app.payload.response.trading.DepositResponse;
import com.pina.app.payload.response.trading.PortfolioPct;
import com.pina.app.payload.response.trading.PortfolioResponse;
import com.pina.app.payload.response.trading.Withdraw;
import com.pina.app.payload.response.trading.WithdrawResponse;
import com.pina.app.repository.StockRep;
import com.pina.app.repository.UserRep;
import com.pina.app.util.StringGenerator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hp
 */
@Service("RealTradingService")
public class RealTradingServiceImpl implements RealTradingService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    UserRep userRepository;

    @Autowired
    StockService stockService;

    @Autowired
    private StockRep stockRepository;

    Map<Long, String> grantedMap = new HashMap<>();
    Map<String, Long> grantedMapInverse = new HashMap<>();

    @Override
    public ResponseEntity<?> loginPin(LoginPinRequest body, long userId) {
        Optional<User> optUser = userRepository.findById(userId);
        User user = null;
        if (optUser.isPresent()) {
            user = optUser.get();
            boolean valid = passwordEncoder.matches(body.getPin(), user.getPin());
            if (!valid) {
                return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Pin not valid!", null),
                        HttpStatus.BAD_REQUEST);
            } else {
                grantedMap.put(user.getId(), StringGenerator.hexEncode());
                grantedMapInverse.put(grantedMap.get(user.getId()), user.getId());
            }
        } else {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.UNAUTHORIZED.asText(), "Not valid user!", null),
                    HttpStatus.UNAUTHORIZED);
        }

        LoginPinResponse result = new LoginPinResponse();
        result.setAccessToken(grantedMap.get(user.getId()));
        return ResponseEntity.ok(new ApiResponse(true, null, "login pin success", result));

    }

    @Override
    public ResponseEntity<?> logoutPin(long userId) {
        grantedMap.remove(userId);
        return ResponseEntity.ok(new ApiResponse(true, null, "logout pin success", null));
    }

    @Override
    public ResponseEntity<?> checkSession(PinCheckRequest body) {
        Optional<User> optUser = userRepository.findById(body.getUserId());
        User user = null;
        if (optUser.isPresent()) {
            user = optUser.get();
            boolean valid = passwordEncoder.matches(body.getPin(), user.getPin());
            if (!valid) {
                return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Pin not valid!", null),
                        HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.UNAUTHORIZED.asText(), "Not valid user!", null),
                    HttpStatus.UNAUTHORIZED);
        }

        if (grantedMap.containsKey(user.getId())) {
            LoginPinResponse result = new LoginPinResponse();
            result.setAccessToken(grantedMap.get(user.getId()));
            return ResponseEntity.ok(new ApiResponse(true, null, "check session success", result));
        }

        return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Session not found!", null),
                HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<?> checkToken(String token) {
        Optional<User> userOpt = null;
        if (grantedMapInverse.containsKey(token)) {
            userOpt = userRepository.findById(grantedMapInverse.get(token));
            if (!userOpt.isPresent()) {
                return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "User not found!", null),
                        HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "User not found!", null),
                    HttpStatus.BAD_REQUEST);
        }

        User user = userOpt.get();
        CheckTokenResponse res = new CheckTokenResponse();
        res.setEmail(user.getEmail());
        res.setUserName(user.getUserName());
        return ResponseEntity.ok(new ApiResponse(true, null, "success", res));
    }

    @Override
    public ResponseEntity<?> createPin(PinAddRequest body, long userId) {
        Optional<User> optUser = userRepository.findById(userId);
        if (optUser.isPresent()) {
            User user = optUser.get();
            if (user.getRealAccountActive()) {
                if (!user.getPinCreated()) {
                    user.setPin(bCryptPasswordEncoder.encode(body.getPin()));
                    user.setPinCreated(true);
                    userRepository.save(user);
                } else {
                    return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "User has created pin!", null),
                            HttpStatus.BAD_REQUEST);
                }
            } else {
                return new ResponseEntity(new ApiResponse(false, HttpStatusCode.UNAUTHORIZED.asText(), "User not yet active!", null),
                        HttpStatus.UNAUTHORIZED);
            }

        } else {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.UNAUTHORIZED.asText(), "Not valid user!", null),
                    HttpStatus.UNAUTHORIZED);
        }
        return ResponseEntity.ok(new ApiResponse(true, null, "login pin created successfuly", null));
    }

    @Override
    public ResponseEntity<?> getPortfolio(long userId) {
        if (grantedMap.containsKey(userId)) {

            PortfolioPct portfolioPct = new PortfolioPct();
            portfolioPct.setStockPct(70);
            portfolioPct.setReksadanaPct(20);
            portfolioPct.setCopyFundPct(10);

            List<String> stocks = new ArrayList<>();
            stocks.add("WSKT");
            stocks.add("ADHI");
            stocks.add("ADRO");

            List<Stock> stockList = stockRepository.findByCodeIn(stocks);

            PortfolioResponse res = new PortfolioResponse();
            res.setAvailableCash(500000);
            res.setBalance(1000000);
            res.setValue(2000000000);
            res.setProfit(1000000);
            res.setProfitPct(10.72);
            res.setPortfolioPct(portfolioPct);
            res.setStockRecommendation(stockList);

            return ResponseEntity.ok(new ApiResponse(true, null, "", res));
        }

        return new ResponseEntity(new ApiResponse(false, HttpStatusCode.UNAUTHORIZED.asText(), "Please Login PIN!", null),
                HttpStatus.UNAUTHORIZED);

    }

    @Override
    public ResponseEntity<?> getDeposit(long userId) {
        if (grantedMap.containsKey(userId)) {
            DepositResponse res = new DepositResponse();
            res.setBankAccountName("John Doe");
            res.setBankName("BCA");
            res.setBankAccountNo("1234354534343");
            res.setBankLogoUrl("BASE_URL/images/avatar/63a7200b4926266e3d3dc2385fd77bc83ef39af4.png");

            res.setCashAvailable(100000000000d);
            List<Deposit> depositHistory = new ArrayList<>();

            Deposit deposit = new Deposit();
            deposit.setAmount(1000000);
            deposit.setBankName("Bank BCA (ATM BCA)");
            deposit.setDepositDate("21 Aug 2018 . 05:00 am");
            depositHistory.add(deposit);

            deposit = new Deposit();
            deposit.setAmount(500000);
            deposit.setBankName("Bank BCA (ATM BCA)");
            deposit.setDepositDate("21 Aug 2018 . 05:00 am");
            depositHistory.add(deposit);

            res.setDepositHistory(depositHistory);

            return ResponseEntity.ok(new ApiResponse(true, null, "", res));
        }

        return new ResponseEntity(new ApiResponse(false, HttpStatusCode.UNAUTHORIZED.asText(), "Please Login PIN!", null),
                HttpStatus.UNAUTHORIZED);
    }

    @Override
    public ResponseEntity<?> getWithdraw(long userId) {
        if (grantedMap.containsKey(userId)) {
            WithdrawResponse res = new WithdrawResponse();
            res.setAvailableCash(2000000);
            res.setCash(1000000);
            res.setBankAccountName("John Doe");
            res.setBankName("BCA");
            res.setBankAccountNo("1234354534343");
            res.setBankLogoUrl("BASE_URL/images/avatar/63a7200b4926266e3d3dc2385fd77bc83ef39af4.png");

            List<Withdraw> withdrawHistory = new ArrayList<>();

            Withdraw withdraw = new Withdraw();
            withdraw.setAmount(500000);
            withdraw.setStatus("ACCEPTED");
            withdraw.setBankAccountNo("123545657668");
            withdraw.setBankName("Bank BCA");
            withdraw.setBankAccountName("John Doe");
            withdraw.setWithdrawDate("21 Aug 2018 . 05:00 am");
            withdrawHistory.add(withdraw);

            withdraw = new Withdraw();
            withdraw.setAmount(500000);
            withdraw.setStatus("COMPLETED");
            withdraw.setBankAccountNo("123545657668");
            withdraw.setBankName("Bank BCA");
            withdraw.setBankAccountName("John Doe");
            withdraw.setWithdrawDate("21 Aug 2018 . 05:00 am");
            withdrawHistory.add(withdraw);

            res.setWithdrawHistory(withdrawHistory);

            return ResponseEntity.ok(new ApiResponse(true, null, "", res));
        }

        return new ResponseEntity(new ApiResponse(false, HttpStatusCode.UNAUTHORIZED.asText(), "Please Login PIN!", null),
                HttpStatus.UNAUTHORIZED);

    }

    @Override
    public ResponseEntity<?> reqWithdraw(WithdrawRequest body, long userId) {
        if (grantedMap.containsKey(userId)) {
            return ResponseEntity.ok(new ApiResponse(true, null, "request withdraw success", null));
        }

        return new ResponseEntity(new ApiResponse(false, HttpStatusCode.UNAUTHORIZED.asText(), "Please Login PIN!", null),
                HttpStatus.UNAUTHORIZED);
    }

}
