/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.google.firebase.auth.ExportedUserRecord;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.ListUsersPage;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.auth.UserRecord.CreateRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.pina.app.model.User;
import com.pina.app.payload.NotificationMsg;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hp
 */
@Service("firebaseService")
public class FirebaseService {

    @Autowired
    private DatabaseReference databaseReff;

    public void sendSpesificTopic(String token) throws FirebaseMessagingException {
        String topic = "friendly_engage";
        AndroidConfig androidConfig = AndroidConfig.builder()
                .setNotification(AndroidNotification.builder()
                        .setTitle("hello pina!")
                        .setIcon("stock_ticker_update")
                        .setColor("#f45342")
                        .setBody("Hello apa kabar")
                        .build())
                .build();

        // See documentation on defining a message payload.
        Message message = Message.builder()
                .setNotification(new Notification(
                        "#title Registration Success",
                        "#body Hello, thank you for completed registration"))
                .setTopic(topic)
                //.setToken(token)
                .setAndroidConfig(androidConfig)
                .build();

        // Send a message to devices subscribed to the combination of topics
        // specified by the provided condition.
        String response = FirebaseMessaging.getInstance().send(message);
        // Response is a message ID string.
        System.out.println("Successfully sent message: " + response);

    }

    public void sendNotification(String token) throws FirebaseMessagingException {
        String condition = "'stock-GOOG' in topics || 'industry-tech' in topics";

        // See documentation on defining a message payload.
        Message message = Message.builder()
                .setNotification(new Notification(
                        "#title Registration Success",
                        "#body Hello, thank you for completed registration"))
                //.setCondition(condition)
                .setToken(token)
                .build();

        // Send a message to devices subscribed to the combination of topics
        // specified by the provided condition.
        String response = FirebaseMessaging.getInstance().send(message);
        // Response is a message ID string.
        System.out.println("Successfully sent message: " + response);

    }

    public void sendToSpesificUser(String token) throws FirebaseMessagingException {
        // This registration token comes from the client FCM SDKs.
        String registrationToken = token;
        // See documentation on defining a message payload.
        Message message = Message.builder()
                .putData("score", "850")
                .putData("time", "2:45")
                .setToken(registrationToken)
                .build();
        // Send a message to the device corresponding to the provided
        // registration token.
        String response = FirebaseMessaging.getInstance().send(message);
        // Response is a message ID string.
        System.out.println("Successfully sent message: " + response);

    }

    //get list token firebase cloud messaging token
    public void getListFCMToken() {
        // Get a reference to our posts
        databaseReff.getDatabase().getReference("fcmTokens").addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
                map.keySet().forEach((key) -> {
//                    try {
//                        //sendToSpesificUser(key);
//                        sendNotification(key);
//                    } catch (FirebaseMessagingException ex) {
//                        Logger.getLogger(FirebaseService.class.getName()).log(Level.SEVERE, null, ex);
//                    }
                    System.out.println("FCM Token : " + key);
                });
            }

            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    public void listAllUsers() throws InterruptedException, ExecutionException {
        // [START list_all_users]
        // Start listing users from the beginning, 1000 at a time.
        ListUsersPage page = FirebaseAuth.getInstance().listUsersAsync(null).get();
        while (page != null) {
            for (ExportedUserRecord user : page.getValues()) {
                System.out.println("User: " + user.getUid());
            }
            page = page.getNextPage();
        }

        // Iterate through all users. This will still retrieve users in batches,
        // buffering no more than 1000 users in memory at a time.
        page = FirebaseAuth.getInstance().listUsersAsync(null).get();
        for (ExportedUserRecord user : page.iterateAll()) {
            System.out.println("User: " + user.getUid());
        }
        // [END list_all_users]
    }

    public void createUserWithUid(User user) throws InterruptedException, ExecutionException {
        String uid = "" + user.getId();
        UserRecord u = null;
        try {
            u = FirebaseAuth.getInstance().getUser(uid);
        } catch (FirebaseAuthException ex) {
            Logger.getLogger(FirebaseService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (u == null) {
            // [START create_user_with_uid]
            CreateRequest request = new CreateRequest()
                    .setUid(uid)
                    .setEmail(user.getEmail())
                    //.setPhoneNumber(user.getUserProfile().getPhoneNo())
                    .setDisplayName(user.getName());

            //.setEmailVerified(false)
            //.setPassword("secretPassword")
            //.setPhotoUrl("http://www.example.com/12345678/photo.png");
            //.setDisabled(false);
            UserRecord userRecord = FirebaseAuth.getInstance().createUserAsync(request).get();
            System.out.println("Successfully created new user: " + userRecord.getUid());
            // [END create_user_with_uid]
        }
    }

    public String createCustomToken(String uid) throws InterruptedException, ExecutionException {
        String customToken = FirebaseAuth.getInstance().createCustomTokenAsync(uid).get();
        // Send token back to client
        System.out.println("Created custom token: " + customToken);
        return customToken;
    }

    public void sendToAllPlatform(NotificationMsg msg) throws FirebaseMessagingException {

        String topic = "pina_notification";
        Message message = Message.builder()
                .setNotification(new Notification(
                        msg.getTitle(),
                        msg.getBody()))
                .setAndroidConfig(AndroidConfig.builder()
                        .setTtl(3600 * 1000) // 1 hour in milliseconds
                        .setPriority(AndroidConfig.Priority.HIGH)
                        .setNotification(AndroidNotification.builder()
                                .setTitle(msg.getTitle())
                                .setBody(msg.getBody())
                                .setIcon("icon_deposit")
                                .setColor("#f45342")
                                .build())
                        .build())
                .setTopic(topic)
                .putData("title", msg.getTitle())
                .putData("body", msg.getBody())
                .putData("longText", msg.getLongText())
                .putData("path", msg.getPath())
                .putData("url", msg.getUrl())
                .build();
        String response = FirebaseMessaging.getInstance().send(message);
        // Response is a message ID string.
        System.out.println("Successfully sent message: " + response);
    }

    public void sendToAllPlatform(String token, NotificationMsg msg) throws FirebaseMessagingException {

        String topic = "pina_notification";
        Message message = Message.builder()
                .setNotification(new Notification(
                        msg.getTitle(),
                        msg.getBody()))
                .setAndroidConfig(AndroidConfig.builder()
                        .setTtl(3600 * 1000) // 1 hour in milliseconds
                        .setPriority(AndroidConfig.Priority.HIGH)
                        .setNotification(AndroidNotification.builder()
                                .setTitle(msg.getTitle())
                                .setBody(msg.getBody())
                                .setIcon("icon_deposit")
                                .setColor("#f45342")
                                .build())
                        .build())
                .setToken(token)
                .putData("title", msg.getTitle())
                .putData("body", msg.getBody())
                .putData("longText", msg.getLongText())
                .putData("path", msg.getPath())
                .putData("url", msg.getUrl())
                .build();
        String response = FirebaseMessaging.getInstance().send(message);
        // Response is a message ID string.
        System.out.println("Successfully sent message: " + response);
    }

    public void sendNotificationToAll(NotificationMsg msg) {
        try {
            sendToAllPlatform(msg);
        } catch (FirebaseMessagingException ex) {
            Logger.getLogger(FirebaseService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendNotificationByUserId(String userId, NotificationMsg msg) {

        databaseReff.getDatabase().getReference("fcmTokens").addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
                map.keySet().forEach((key) -> {
                    String uid = map.get(key);
                    if (uid.equals(userId)) {
                        try {
                            sendToAllPlatform(key, msg);
                        } catch (FirebaseMessagingException ex) {
                            Logger.getLogger(FirebaseService.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        return;
                    }

                    System.out.println("FCM Token : " + key);
                });
            }

            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    public void newsFeedSaveChange(long userId, Object message) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("news_feeds_user_"+userId);
        DatabaseReference usersRef = ref.child("records");
        usersRef.setValueAsync(message);
    }
}
