/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.exception.ResourceNotFoundException;
import com.pina.app.model.Stock;
import com.pina.app.model.User;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pina.app.repository.StockRep;
import com.pina.app.repository.UserRep;

/**
 *
 * @author Hp
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRep userRepository;

	@Autowired
	private StockRep stockRepository;

	@Override
	public Optional<User> findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public Optional<User> findUserByVerificationCode(String code) {
		return userRepository.findByVerificationCode(code);
	}

	@Override
	public void save(User user) {
		userRepository.save(user);
	}

	@Override
	public Optional<User> findUserByVerificationCodeAndEmail(String code, String email) {
		return userRepository.findUserByVerificationCodeAndEmail(code, email);
	}

	@Override
	public void followUser(Long userId, List<Long> listUserId) {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
		List<User> ls = userRepository.findByIdIn(listUserId);
		Set<User> listFollowUser = user.getFollowUsers();
		ls.forEach(u -> {
			listFollowUser.add(u);
		});

		user.setFollowUsers(listFollowUser);
		User s = userRepository.save(user);
	}

	@Override
	public void unfollowUser(Long userId, List<Long> listUserId) {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
		List<User> ls = userRepository.findByIdIn(listUserId);
		Set<User> listFollowUser = user.getFollowUsers();
		ls.forEach(u -> {
			listFollowUser.remove(u);
		});

		user.setFollowUsers(listFollowUser);
		User s = userRepository.save(user);
	}

	@Override
	public void followStock(Long userId, List<Long> listStockId) {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
		List<Stock> ls = stockRepository.findByIdIn(listStockId);
		Set<Stock> listFollowStock = user.getFollowStocks();
		ls.forEach(u -> {
			listFollowStock.add(u);
		});

		user.setFollowStocks(listFollowStock);
		User s = userRepository.save(user);

	}

	@Override
	public void unfollowStock(Long userId, List<Long> listStockId) {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
		List<Stock> ls = stockRepository.findByIdIn(listStockId);
		Set<Stock> listFollowStock = user.getFollowStocks();
		ls.forEach(u -> {
			listFollowStock.remove(u);
		});

		user.setFollowStocks(listFollowStock);
		User s = userRepository.save(user);

	}

}
