package com.pina.app.service;

import java.util.List;
import java.util.Optional;

import com.pina.app.model.Stock;

public interface StockService {
	
	public Stock  findById(Long id);
	public Stock findByCode(String code);
	public Stock findByName(String name);
	public void saveStock(Stock stock);
	public void updateStock(Stock stock);
	void deleteStockById(Long id);
	public List<Stock> findAllStocks();
	public boolean isStockExist(Stock user);
	
	public void followStock(Long userId, List<Long> listStockId);
	public void unfollowStock(Long userId, List<Long> listStockId);
}
