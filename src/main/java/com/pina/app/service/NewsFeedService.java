/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.payload.request.CommentCreateRequest;
import com.pina.app.payload.request.NewsFeedCreateRequest;
import com.pina.app.security.UserPrincipal;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author Hp
 */
public interface NewsFeedService {

    ResponseEntity<?> createNewsFeed(NewsFeedCreateRequest body, UserPrincipal currentUser);

    ResponseEntity<?> deleteNewsFeed(long id);

    ResponseEntity<?> getAllNewsFeed(long userId, long lastId);

    ResponseEntity<?> getAllCommentNewsFeed(long newsFeedId, long lastId);

    ResponseEntity<?> getAllNewsFeedByUserId(long id);

    ResponseEntity<?> createComment(CommentCreateRequest body, UserPrincipal currentUser);

    ResponseEntity<?> reload(long userId);

}
