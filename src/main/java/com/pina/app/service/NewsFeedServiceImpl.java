/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.constants.HttpStatusCode;
import com.pina.app.model.Comment;
import com.pina.app.model.NewsFeed;
import com.pina.app.model.NewsFeedAccess;
import com.pina.app.model.NewsFeedType;
import com.pina.app.model.User;
import com.pina.app.payload.request.CommentCreateRequest;
import com.pina.app.payload.request.NewsFeedCreateRequest;
import com.pina.app.payload.response.ApiResponse;
import com.pina.app.payload.response.newsfeed.NFComment;
import com.pina.app.payload.response.newsfeed.NFPost;
import com.pina.app.payload.response.newsfeed.NFPostComment;
import com.pina.app.payload.response.newsfeed.NFPostShare;
import com.pina.app.payload.response.newsfeed.NFUser;
import com.pina.app.payload.response.newsfeed.NewsFeedPost;
import com.pina.app.repository.CommentRep;
import com.pina.app.repository.NewsFeedRep;
import com.pina.app.repository.UserRep;
import com.pina.app.security.UserPrincipal;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Cursor;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hp
 */
@Service("NewsFeedService")
public class NewsFeedServiceImpl implements NewsFeedService {

    @Autowired
    private UserRep userRepository;

    @Autowired
    private NewsFeedRep newsFeedRep;

    @Autowired
    private CommentRep commentRep;

    @Autowired
    private FirebaseService firbaseService;

    @Override
    public ResponseEntity<?> createNewsFeed(NewsFeedCreateRequest body, UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false,
                    HttpStatusCode.BAD_REQUEST.asText(),
                    "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }

        NewsFeedType type = NewsFeedType.fromValue(body.getType());
        NewsFeedAccess access = NewsFeedAccess.fromValue(body.getAccess());

        User user = userOpt.get();
        NewsFeed newsFeed = new NewsFeed();
        newsFeed.setAccess(access);
        newsFeed.setType(type);
        newsFeed.setParentId(body.getShareFromId());
        newsFeed.setContent(body.getContent());
        newsFeed.setAttachmentURLs(body.getAttchmentURLs());
        newsFeed.setPhotoURLs(body.getPhotoURLs());
        newsFeed.setUser(user);

        if (type == NewsFeedType.SHARE) {
            newsFeed.getUserShares().add(user);
        }

        newsFeed = newsFeedRep.save(newsFeed);

        //rethinkdb
        //--------------------------------------------------------------------
        //insert new post
        insertNewsFeed(newsFeed);
        //kirim newsfeed kesemua followernya
        broadcastToFollower(user, newsFeed);
        //--------------------------------------------------------------------

        return ResponseEntity.created(null).body(
                new ApiResponse(true, null, "NewsFeed Created Successfully", null));
    }

    @Override
    public ResponseEntity<?> deleteNewsFeed(long id) {
        Optional<NewsFeed> newFeedOpt = newsFeedRep.findById(id);
        if (!newFeedOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false,
                    HttpStatusCode.BAD_REQUEST.asText(),
                    "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }
        NewsFeed newFeed = newFeedOpt.get();
        newsFeedRep.delete(newFeed);
        return ResponseEntity.ok(new ApiResponse(true, null, "NewsFeed delete Successfully", null));
    }

    //get semua post yg di follow oleh user, ambil 20 row teratas by id
    @Override
    public ResponseEntity<?> getAllNewsFeed(long userId, long lastId) {
        return ResponseEntity.ok(new ApiResponse(true, null, "", getListNewsFeed(userId, lastId)));
    }

    @Override
    public ResponseEntity<?> getAllCommentNewsFeed(long newsFeedId, long lastId) {
        return ResponseEntity.ok(new ApiResponse(true, null, "", getListComment(newsFeedId, lastId)));
    }

    @Override
    public ResponseEntity<?> getAllNewsFeedByUserId(long id) {
        return null;
    }

    @Override
    public ResponseEntity<?> createComment(CommentCreateRequest body, UserPrincipal currentUser) {
        Optional<User> userOpt = userRepository.findById(currentUser.getId());
        if (!userOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false,
                    HttpStatusCode.BAD_REQUEST.asText(),
                    "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }

        Optional<NewsFeed> newsFeedOpt = newsFeedRep.findById(body.getNewsFeedId());
        if (!newsFeedOpt.isPresent()) {
            return new ResponseEntity(new ApiResponse(false,
                    HttpStatusCode.BAD_REQUEST.asText(),
                    "bad request", null),
                    HttpStatus.BAD_REQUEST);
        }

        NewsFeed commentedNewsFeed = newsFeedOpt.get();
        User user = userOpt.get();

        //create comment
        Comment comment = new Comment();
        comment.setContent(body.getContent());
        comment.setUser(user);

        if (body.getReplyCommentId() != null && body.getReplyCommentId() > 0) {
            Optional<Comment> parentOpt = commentRep.findById(body.getReplyCommentId());
            if (parentOpt.isPresent()) {
                comment.setParent(parentOpt.get());
            }
        }
        comment = commentRep.save(comment);
        commentedNewsFeed.getComments().add(comment);
        commentedNewsFeed = newsFeedRep.save(commentedNewsFeed);

        //create post newsfeed with type comment
        NewsFeed newsFeed = new NewsFeed();
        newsFeed.setAccess(NewsFeedAccess.PUBLIC);
        newsFeed.setType(NewsFeedType.COMMENT);
        newsFeed.setParentId(commentedNewsFeed.getId());
        newsFeed.setUser(user);
        newsFeed = newsFeedRep.save(newsFeed);

        //rethinkdb
        //-----------------------------------------------------------------------
        //insert comment 
        insertComment(commentedNewsFeed.getId(), comment);
        //update post yg dikomentari
        updateNewsFeed(commentedNewsFeed);
        //insert newfeed untuk comment sndiri
        insertNewsFeed(newsFeed);
        //kirim newsfeed kesemua followernya
        broadcastToFollower(user, newsFeed);
        //-----------------------------------------------------------------------

        return ResponseEntity.ok(new ApiResponse(true, null, "Post Comment Successfully", null));
    }

    @Override
    public ResponseEntity<?> reload(long userId) {
        Optional<User> userOpt = userRepository.findById(userId);
        if (userOpt.isPresent()) {
            deleteTableNewsFeed(userId);
            List<NewsFeed> list = userOpt.get().getNewsFeeds();
            list.forEach((item) -> {
                insertNewsFeed(item);
            });
        }
        return ResponseEntity.ok(new ApiResponse(true, null, "reload Successfully", null));
    }

    @Async("threadPoolTaskExecutor")
    private void broadcastToFollower(User user, NewsFeed newsfeed) {
        Set<User> users = user.getFollowUsers();
        users.forEach((u) -> {
            if (newsfeed.getAccess() == NewsFeedAccess.PUBLIC) {
                insertNewsFeedToFollower(u.getId(), newsfeed);
            }
        });
    }

    @Async("threadPoolTaskExecutor")
    private void broadcastUpdateToFollower(User user, NewsFeed newsfeed) {
        Set<User> users = user.getFollowUsers();
        users.forEach((u) -> {
            updateNewsFeedToFollower(u.getId(), newsfeed);
        });
    }
    ///=========================================================================
    public static final RethinkDB r = RethinkDB.r;
    public static final String DB_NAME = "pina";
    public static final String TBL_NEWS_FEEDS = "news_feeds";
    public static final String TBL_NEWS_FEED_COMMENTS = "news_feed_comments";

    public static Connection getConnection() {
        Connection conn = r.connection().hostname("localhost").port(28015).connect();
        List<String> ls = r.dbList().run(conn);

        boolean exist = false;
        for (String name : ls) {
            if (name.equals(DB_NAME)) {
                exist = true;
            }
        }
        if (!exist) {
            r.dbCreate(DB_NAME).run(conn);
        }

        conn.use(DB_NAME);

        return conn;
    }

    private void deleteTableNewsFeed(long userId) {
        List<String> tables = r.tableList().run(getConnection());
        Optional<String> tableOpt = tables
                .stream().parallel()
                .filter(table -> table.equalsIgnoreCase(TBL_NEWS_FEEDS))
                .findAny();

        if (tableOpt.isPresent()) {
            r.table(TBL_NEWS_FEEDS).delete().run(getConnection());
        }
    }

    private void updateNewsFeed(NewsFeed record) {
        r.table(TBL_NEWS_FEEDS)
                .filter(r.hashMap("id", record.getId()))
                .update(parseNewsFeed(record))
                .run(getConnection());

        r.table(TBL_NEWS_FEEDS + "_" + record.getUser().getId())
                .filter(r.hashMap("id", record.getId()))
                .update(r.hashMap("id", record.getId())
                        .with("createdAt", record.getFormattedCreatedAt())
                        .with("updatedAt", record.getFormattedUpdatedAt()))
                .run(getConnection());
    }

    private void updateNewsFeedToFollower(long userId, NewsFeed record) {
        r.table(TBL_NEWS_FEEDS + "_" + userId)
                .filter(r.hashMap("id", record.getId()))
                .update(r.hashMap("id", record.getId())
                        .with("createdAt", record.getFormattedCreatedAt())
                        .with("updatedAt", record.getFormattedUpdatedAt()))
                .run(getConnection());
    }

    private void insertNewsFeedToFollower(long userId, NewsFeed record) {
        List<String> tables = r.tableList().run(getConnection());
        Optional<String> tableOpt = tables
                .stream().parallel()
                .filter(table -> table.equalsIgnoreCase(TBL_NEWS_FEEDS + "_" + userId))
                .findAny();

        if (!tableOpt.isPresent()) {
            r.tableCreate(TBL_NEWS_FEEDS + "_" + userId).run(getConnection());
        }
        r.table(TBL_NEWS_FEEDS + "_" + userId)
                .insert(r.hashMap("id", record.getId())
                        .with("createdAt", record.getFormattedCreatedAt())
                        .with("updatedAt", record.getFormattedUpdatedAt()))
                .run(getConnection());
        
        firbaseService.newsFeedSaveChange(userId, 
                r.table(TBL_NEWS_FEEDS).get(record.getId()).run(getConnection()));
    }

    private void insertNewsFeed(NewsFeed record) {
        long userId = record.getUser().getId();
        List<String> tables = r.tableList().run(getConnection());
        Optional<String> tableOpt = tables
                .stream().parallel()
                .filter(table -> table.equalsIgnoreCase(TBL_NEWS_FEEDS))
                .findAny();

        if (!tableOpt.isPresent()) {
            r.tableCreate(TBL_NEWS_FEEDS).run(getConnection());
            //r.table(TBL_NEWS_FEEDS).indexCreate("index_name").run(getConnection());
        }

        r.table(TBL_NEWS_FEEDS)
                .insert(parseNewsFeed(record))
                .run(getConnection());

        tableOpt = tables
                .stream().parallel()
                .filter(table -> table.equalsIgnoreCase(TBL_NEWS_FEEDS + "_" + userId))
                .findAny();

        if (!tableOpt.isPresent()) {
            r.tableCreate(TBL_NEWS_FEEDS + "_" + userId).run(getConnection());
        }

        r.table(TBL_NEWS_FEEDS + "_" + userId)
                .insert(r.hashMap("id", record.getId())
                        .with("createdAt", record.getFormattedCreatedAt())
                        .with("updatedAt", record.getFormattedUpdatedAt()))
                .run(getConnection());
        
        firbaseService.newsFeedSaveChange(userId, 
                r.table(TBL_NEWS_FEEDS).get(record.getId()).run(getConnection()));

    }

    @Async("threadPoolTaskExecutor")
    private void insertComment(long newsFeedId, Comment record) {
        List<String> tables = r.tableList().run(getConnection());
        Optional<String> tableOpt = tables
                .stream().parallel()
                .filter(table -> table.equalsIgnoreCase(TBL_NEWS_FEED_COMMENTS))
                .findAny();

        if (!tableOpt.isPresent()) {
            r.tableCreate(TBL_NEWS_FEED_COMMENTS).run(getConnection());
        }

        //insert root
        if (record.getParent() == null) {
            r.table(TBL_NEWS_FEED_COMMENTS)
                    .insert(parseComment(newsFeedId, record))
                    .run(getConnection());
        } else {
            Comment rootRecord = getRootComment(record);
            r.table(TBL_NEWS_FEED_COMMENTS)
                    .filter(r.hashMap("id", rootRecord.getId()))
                    .update(parseComment(newsFeedId, rootRecord))
                    .run(getConnection());
        }

    }

    private final static int LIMIT = 20;

    private List<Object> getListNewsFeed(long userId, long lastId) {
        List<Object> result = new ArrayList<>();
        String tblNewFeedUser = TBL_NEWS_FEEDS + "_" + userId;
        List<String> tables = r.tableList().run(getConnection());
        Optional<String> tableOpt = tables
                .stream().parallel()
                .filter(table -> table.equalsIgnoreCase(tblNewFeedUser))
                .findAny();

        if (tableOpt.isPresent()) {
            Cursor cursor = null;
            if (lastId > 0) {
                cursor = r.table(tblNewFeedUser)
                        .orderBy().optArg("index", r.desc("id"))
                        .innerJoin(r.table(TBL_NEWS_FEEDS),
                                (row_user, row_master) -> row_user.g("id").eq(row_master.g("id")))
                        .zip()
                        .merge(row -> r.hashMap("post", r.hashMap("parent", r.table(TBL_NEWS_FEEDS).get(row.g("post").g("parentId")))))
                        .filter(row -> row.g("id").lt(lastId))
                        .limit(LIMIT)
                        .run(getConnection());
            } else {
                cursor = r.table(tblNewFeedUser)
                        .orderBy().optArg("index", r.desc("id"))
                        .innerJoin(r.table(TBL_NEWS_FEEDS),
                                (row_user, row_master) -> row_user.g("id").eq(row_master.g("id")))
                        .zip()
                        .merge(row -> r.hashMap("post", r.hashMap("parent", r.table(TBL_NEWS_FEEDS).get(row.g("post").g("parentId")))))
                        .limit(LIMIT)
                        .run(getConnection());
            }

            for (Object doc : cursor) {
                result.add(doc);
                System.out.println(doc);
            }
        }
        return result;
    }

    private List<Object> getListComment(long newsFeedId, long lastId) {
        List<Object> result = new ArrayList<>();
        List<String> tables = r.tableList().run(getConnection());
        Optional<String> tableOpt = tables
                .stream().parallel()
                .filter(table -> table.equalsIgnoreCase(TBL_NEWS_FEED_COMMENTS))
                .findAny();

        if (tableOpt.isPresent()) {
            Cursor cursor = null;
            if (lastId > 0) {
                cursor = r.table(TBL_NEWS_FEED_COMMENTS)
                        .orderBy().optArg("index", r.desc("id"))
                        .filter(row -> row.g("id").lt(lastId).and(row.g("newsFeedId").eq(newsFeedId)))
                        .limit(LIMIT)
                        .run(getConnection());
            } else {
                cursor = r.table(TBL_NEWS_FEED_COMMENTS)
                        .orderBy().optArg("index", r.desc("id"))
                        .filter(row -> row.g("newsFeedId").eq(newsFeedId))
                        .limit(LIMIT)
                        .run(getConnection());
            }

            for (Object doc : cursor) {
                result.add(doc);
                System.out.println(doc);
            }

        }

        return result;
    }

    @Scheduled(fixedRate = 1000)
    public void backgroundTask() {
        Cursor changeCursor = r.table(TBL_NEWS_FEEDS).changes().run(getConnection());
        for (Object change : changeCursor) {
            System.out.println(change);
        }

    }

    //mapping object
    private NewsFeedPost parseNewsFeed(NewsFeed record) {

        String label = "";
        Object post = null;
        List<NFComment> listComment = new ArrayList<>();
        Optional<Comment> commOpt = commentRep.GetOneByDateLatest(record.getId());

        if (commOpt.isPresent()) {
            Comment comm = commOpt.get();
            List<NFComment> replies = new ArrayList<>();
            NFComment commItem = new NFComment();
            commItem.setContent(comm.getContent());
            commItem.setCreatedAt(comm.getFormattedCreatedAt());
            commItem.setId(comm.getId());
            commItem.setNewsFeedId(record.getId());
            commItem.setReplies(replies);
            commItem.setUpdatedAt(comm.getFormattedUpdatedAt());
            listComment.add(commItem);
        }

        if (null != record.getType()) {
            switch (record.getType()) {
                case POST:
                    NFPost p = new NFPost();
                    p.setParentId(0);
                    p.setAttachmentURLs(record.getAttachmentURLs());
                    p.setComments(listComment);
                    p.setContent(record.getContent());
                    p.setPhotoURLs(record.getPhotoURLs());
                    p.setSumComments(record.getSumComments());
                    p.setSumLikes(record.getSumLikes());
                    p.setSumShares(record.getSumShares());
                    post = p;
                    break;
                case SHARE:
                    label = "membagikan kiriman ";
                    NFPostShare s = new NFPostShare();
                    s.setComments(listComment);
                    s.setContent(record.getContent());
                    s.setParentId(record.getParentId());
                    s.setSumComments(record.getSumComments());
                    s.setSumLikes(record.getSumLikes());
                    s.setSumShares(record.getSumShares());
                    post = s;
                    break;
                case COMMENT:
                    label = "mengkomentari kiriman ";
                    NFPostComment c = new NFPostComment();
                    c.setParentId(record.getParentId());
                    post = c;
                    break;
                default:
                    break;
            }
        }

        NFUser user = new NFUser();
        if (record.getUser() != null) {
            user.setId(record.getUser().getId());
            user.setName(record.getUser().getName());
            user.setInfluencer(record.getUser().getInfluencer());
            user.setImgAvatarUrl(record.getUser().getUserProfile().getImgAvatarUrl());
        }
        NewsFeedPost result = new NewsFeedPost();
        result.setId(record.getId());
        result.setType(record.getType().getValue());
        result.setLabel(label);
        result.setPost(post);
        result.setCreatedAt(record.getFormattedCreatedAt());
        result.setUser(user);
        result.setUpdatedAt(record.getFormattedUpdatedAt());
        return result;
    }

    private NFComment parseComment(long newsFeedId, Comment record) {
        List<NFComment> listComment = new ArrayList<>();
        List<Comment> ls = record.getChildren();
        if (ls != null && ls.size() > 0) {
            ls.forEach((c) -> {
                listComment.add(parseComment(newsFeedId, c));
            });
        }

        Long parentId = null;
        if (record.getParent() != null) {
            parentId = record.getParent().getId();
        }
        NFComment result = new NFComment();
        result.setContent(record.getContent());
        result.setCreatedAt(record.getFormattedCreatedAt());
        result.setId(record.getId());
        result.setNewsFeedId(newsFeedId);
        result.setReplies(listComment);
        result.setUpdatedAt(record.getFormattedUpdatedAt());

        return result;
    }

    private Comment getRootComment(Comment record) {
        if (record.getParent() == null) {
            return record;
        }
        return getRootComment(record.getParent());
    }

}
