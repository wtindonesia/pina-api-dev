package com.pina.app.service;

import com.pina.app.payload.request.DemoRequest;
import com.pina.app.security.UserPrincipal;

import org.springframework.http.ResponseEntity;

public interface DemoService {
    public ResponseEntity<?> postDemo(DemoRequest body, UserPrincipal currentUser);

    public ResponseEntity<?> getDemoById(UserPrincipal currentUser);

	public ResponseEntity<?> getAllByKey(String key);
}