package com.pina.app.service;

import com.pina.app.exception.BadRequestException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pina.app.exception.ResourceNotFoundException;
import com.pina.app.model.Stock;
import com.pina.app.model.User;
import org.springframework.dao.DataIntegrityViolationException;
import com.pina.app.repository.StockRep;
import com.pina.app.repository.UserRep;

@Service("stockService")
@Transactional
public class StockServiceImpl implements StockService {

    @Autowired
    private UserRep userRepository;

    @Autowired
    private StockRep stockRepository;

    @Override
    public void followStock(Long userId, List<Long> listStockId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
        List<Stock> ls = stockRepository.findByIdIn(listStockId);
        Set<Stock> listFollowUser = user.getFollowStocks();
        ls.forEach(u -> {
            listFollowUser.add(u);
        });

        user.setFollowStocks(listFollowUser);
        // User s = userRepository.save(user);
    }

    @Override
    public void unfollowStock(Long userId, List<Long> listStockId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
        List<Stock> ls = stockRepository.findByIdIn(listStockId);
        Set<Stock> listFollowUser = user.getFollowStocks();
        ls.forEach(u -> {
            listFollowUser.remove(u);
        });

        user.setFollowStocks(listFollowUser);
        // User s = userRepository.save(user);
    }

    @Override
    public Stock findById(Long id) {
        Stock stock = stockRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Stock", "id", id));
        return stock;
    }

    @Override
    public Stock findByCode(String code) {
        return stockRepository.findByCode(code);
    }

    @Override
    public Stock findByName(String name) {
        return stockRepository.findByName(name);
    }

    @Override
    public void saveStock(Stock stock) {
        try {
            Stock s = stockRepository.save(stock);
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("stock already created!");
        }
    }

    @Override
    public void updateStock(Stock stock) {
        saveStock(stock);

    }

    @Override
    public List<Stock> findAllStocks() {
        return stockRepository.findAll();
    }

    @Override
    public boolean isStockExist(Stock stock) {
        return findByCode(stock.getCode()) != null;
    }

    @Override
    public void deleteStockById(Long id) {
        stockRepository.deleteById(id);

    }
}
