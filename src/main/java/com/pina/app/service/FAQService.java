/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pina.app.service;

import com.pina.app.payload.request.FAQRequest;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author Hp
 */
public interface FAQService {

    public ResponseEntity<?> addFAQ(FAQRequest body);

    public ResponseEntity<?> getFAQ(String name);

}
