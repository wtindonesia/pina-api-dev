# PINA Backend
PINA backend built using Sprint Boot 2.0.0-RELEASE and Java8

# Technology Stack

  - Java8
  - Spring 2.0.0-RELEASE
  - PostgreSQL 8 or later
### Technology Stacks

Dillinger uses a number of open source projects to work properly:

* [Spring Boot 2.0.0-RELEASE](https://spring.io/) - blablabla

### Installation
1. Copy and change `config/application-devlocal-example.properties` to `config/application-devlocal.properties`
2. Modify the datasource for PostgreSQL below:
```
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://localhost:5432/<dbname>
spring.datasource.username=<dbuser>
spring.datasource.password=<dbpassword>
```
3. Run this command to build .jar
```sh
$ mvn clean package
```
4. Change permissions `startup_devlocal.sh` to make it executable by running:
```sh
$ chmod +x startup_devlocal.sh
```
6. Run the spring boot using this command 
```sh
$ ./startup_devlocal.sh
```
7. PINA backend will using port `8090`
