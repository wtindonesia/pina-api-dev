#!/bin/bash
mkdir -p logs
mkdir -p pids
mvn clean package
nohup java -jar target/pina-0.0.1-SNAPSHOT.jar --spring.profiles.active=devlocal --spring.config.location=config/ > logs/log.txt 2>&1 &
echo $! > pids/pid.file
tail -f logs/log.txt